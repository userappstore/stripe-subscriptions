const dashboard = require('@userappstore/dashboard')

module.exports = {
  before: beforeRequest,
  get: renderPage,
  post: submitForm
}

async function beforeRequest(req) {
  if (!req.query || !req.query.planid) {
    throw new Error('invalid-planid')
  }
  if (req.session.lockURL === req.url && req.session.unlocked) {
    await global.api.user.subscriptions.CreateSubscription.post(req)
  }
  const plan = await global.api.user.subscriptions.PublishedPlan.get(req)
  req.query.accountid = req.account.accountid
  if (req.success) {
    req.data = { plan }
    return
  }
  const customers = await global.api.user.subscriptions.Customers.get(req)
  req.data = { customers, plan }
}

async function renderPage (req, res, messageTemplate) {
  if (req.success) {
    messageTemplate = 'success'
  } else if (req.error) {
    messageTemplate = req.error
  }
  const doc = dashboard.HTML.parse(req.route.html, req.data.plan, 'plan')
  if (messageTemplate) {
    dashboard.HTML.renderTemplate(doc, null, messageTemplate, 'message-container')
    if (messageTemplate === 'success' || messageTemplate === 'duplicate-subscription') {
      const submitForm = doc.getElementById('submit-form')
      submitForm.parentNode.removeChild(submitForm)
      return dashboard.Response.end(req, res, doc)
    }
  }
  if (req.data.customers && req.data.customers.length) {
    dashboard.HTML.renderList(doc, req.data.customers, 'customer-option', 'customerid')
    if (req.body && req.body.customerid) {
      dashboard.HTML.setSelectedOptionByValue(doc, 'customerid', req.body.customerid)
    }
  }
  return dashboard.Response.end(req, res, doc)
}

async function submitForm(req, res) {
  if (!req.body || !req.body.customerid) {
    return renderPage(req, res)
  }
  let found = false
  for (const customer of req.data.customers) {
    found = customer.id === req.body.customerid
    if (found) {
      if (!customer.default_source) {
        return renderPage(req, res, 'invalid-cardid')
      }
      req.query.customerid = req.body.customerid
      const subscriptions = await global.api.user.subscriptions.CustomerSubscriptions.get(req)
      if (subscriptions && subscriptions.length) {
        for (const subscription of subscriptions) {
          if (subscription.plan.id === req.query.planid) {
            return renderPage(req, res, 'duplicate-subscription')
          }
        }
      }
      break
    }
  }
  if (!found) {
    return renderPage(req, res, 'invalid-customerid')
  }
  req.query.customerid = req.body.customerid
  req.body.planid = req.query.planid
  try {
    await global.api.user.subscriptions.CreateSubscription.post(req)
    if (req.success) {
      return renderPage(req, res, 'success')
    }
    return dashboard.Response.redirect(req, res, '/account/authorize')
  } catch (error) {
    return renderPage(req, res, error.message)
  }
}
