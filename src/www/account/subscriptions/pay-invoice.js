const dashboard = require('@userappstore/dashboard')
module.exports = {
  before: beforeRequest,
  get: renderPage,
  post: submitForm
}

async function beforeRequest (req) {
  if (!req.query || !req.query.invoiceid) {
    throw new Error('invalid-invoiceid')
  }
  if (req.session.lockURL === req.url && req.session.unlocked) {
    await global.api.user.subscriptions.SetInvoicePaid.patch(req)
    if (req.success) {
      return
    }
  }
  const invoice = await global.api.user.subscriptions.Invoice.get(req)
  if (invoice.paid) {
    throw new Error('invalid-invoice')
  }
  req.query.subscriptionid = invoice.subscription || invoice.lines.data[0].subscription
  const subscription = await global.api.user.subscriptions.Subscription.get(req)
  req.query.customerid = subscription.customer
  const customer = await global.api.user.subscriptions.Customer.get(req)
  req.data = { customer, invoice, subscription }
}

async function renderPage (req, res, messageTemplate) {
  if (req.success) {
    messageTemplate = 'success'
  }
  const doc = dashboard.HTML.parse(req.route.html, req.data.invoice, 'invoice')
  if (messageTemplate) {
    dashboard.HTML.renderTemplate(doc, null, messageTemplate, 'message-container')
    if (messageTemplate === 'success') {
      const submitForm = doc.getElementById('submit-form')
      submitForm.parentNode.removeChild(submitForm)
      return dashboard.Response.end(req, res, doc)
    }
  }
  const amount = { amount: req.data.invoice.amount - req.data.invoice.amount_paid, object: 'invoice' }
  dashboard.HTML.renderTemplate(doc, amount, 'amount-template', 'amount-now')
  return dashboard.Response.end(req, res, doc)
}

async function submitForm (req, res) {
  if (!req.body) {
    return renderPage(req, res)
  }
  try {
    req.body.cardid = req.data.customer.default_source
    await global.api.user.subscriptions.SetInvoicePaid.patch(req)
    if (req.success) {
      return renderPage(req, res, 'success')
    }
    return dashboard.Response.redirect(req, res, '/account/authorize')
  } catch (error) {
    return renderPage(req, res, 'unknown-error')
  }
}
