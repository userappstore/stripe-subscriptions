const dashboard = require('@userappstore/dashboard')

module.exports = {
  before: beforeRequest,
  get: renderPage
}

async function beforeRequest (req) {
  if (!req.query || !req.query.customerid) {
    throw new Error('invalid-customerid')
  }
  const customer = await global.api.user.subscriptions.Customer.get(req)
  if (!customer) {
    throw new Error('invalid-customerid')
  }
  const total = await global.api.user.subscriptions.CustomerInvoicesCount.get(req)
  const invoices = await global.api.user.subscriptions.CustomerInvoices.get(req)
  const offset = req.query.offset || 0
  if (invoices && invoices.length) {
    for (const invoice of invoices) {
      if (invoice.total) {
        invoice.totalFormatted = dashboard.Format.money(invoice.total, invoice.currency)
        invoice.date = dashboard.Timestamp.date(invoice.date)
        invoice.period_start = dashboard.Timestamp.date(invoice.lines.data[0].period.start)
        invoice.period_end = dashboard.Timestamp.date(invoice.lines.data[0].period.end)
        invoice.plan_name = invoice.lines.data[0].plan.name
      }
    }
  }
  req.data = { customer, invoices, total, offset }
}

async function renderPage (req, res) {
  const doc = dashboard.HTML.parse(req.route.html, req.data.customer, 'customer')
  if (req.data.invoices && req.data.invoices.length) {
    dashboard.HTML.renderTable(doc, req.data.invoices, 'invoice-row', 'invoices-table')
    if (req.data.total <= global.pageSize) {
      const pageLinks = doc.getElementById('page-links')
      pageLinks.parentNode.removeChild(pageLinks)
    } else {
      dashboard.HTML.renderPagination(doc, req.data.offset, req.data.total)
    }
    const noInvoices = doc.getElementById('no-invoices')
    noInvoices.parentNode.removeChild(noInvoices)
  } else {
    const invoicesTable = doc.getElementById('invoices-table')
    invoicesTable.parentNode.removeChild(invoicesTable)
  }
  return dashboard.Response.end(req, res, doc)
}
