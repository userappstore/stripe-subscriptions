const countries = require('../../../../countries.json')
const countryDivisions = require('../../../../country-divisions.json')
const dashboard = require('@userappstore/dashboard')

module.exports = {
  before: beforeRequest,
  get: renderPage,
  post: submitForm
}

async function beforeRequest (req) {
  if (req.session.lockURL === req.url && req.session.unlocked) {
    req.query = req.query || {}
    req.query.accountid = req.account.accountid
    await global.api.user.subscriptions.CreateCustomerWithCard.post(req)
  }
}

async function renderPage (req, res, messageTemplate) {
  if (req.success) {
    if (req.query.returnURL) {
      return dashboard.Response.redirect(req, res, returnURL)
    }
    return dashboard.Response.redirect(req, res, '/account/subscriptions/billing-profiles')
  }
  let doc
  // customize the form for stripe.js vX (or off)
  const unusedVersions = []
  if (global.stripeJS === false) {
    doc = dashboard.HTML.parse(req.route.html, {}, 'dashboard')
    unusedVersions.push('stripe-v2', 'client-v2', 'stripe-v3', 'client-v3', 'form-v3')
  } else if (global.stripeJS === 2) {
    doc = dashboard.HTML.parse(req.route.html, { dashboardServer: global.dashboardServer, stripePublishableKey: global.stripePublishableKey }, 'dashboard')
    unusedVersions.push('stripe-v3', 'client-v3', 'form-v3')
    res.setHeader('content-security-policy',
      `default-src * 'unsafe-inline'; ` +
      `style-src https://m.stripe.com/ https://m.stripe.network/ https://js.stripe.com/v2/ ${global.dashboardServer}/public/ 'unsafe-inline'; ` +
      `script-src * https://m.stripe.com/ https://m.stripe.network/  https://js.stripe.com/v2/ ${global.dashboardServer}/public/stripe-helper.js 'unsafe-inline' 'unsafe-eval'; ` +
      `frame-src https://m.stripe.com/ https://m.stripe.network/  https://js.stripe.com/ 'unsafe-inline' ; ` +
      `connect-src https://m.stripe.com/ https://m.stripe.network/ https://js.stripe.com/ 'unsafe-inline' ; `)
  } else if (global.stripeJS === 3) {
    doc = dashboard.HTML.parse(req.route.html, { dashboardServer: global.dashboardServer, stripePublishableKey: global.stripePublishableKey }, 'dashboard')
    unusedVersions.push('stripe-v2', 'client-v2', 'form-v2')
    res.setHeader('content-security-policy',
      `default-src * 'unsafe-inline'; ` +
      `style-src https://m.stripe.com/ https://m.stripe.network/ https://js.stripe.com/v3/ https://js.stripe.com/v2/ ${global.dashboardServer}/public/ 'unsafe-inline'; ` +
      `script-src * https://q.stripe.com/ https://m.stripe.com/ https://m.stripe.network/ https://js.stripe.com/v3/ https://js.stripe.com/v2/ ${global.dashboardServer}/public/stripe-helper.js 'unsafe-inline' 'unsafe-eval'; ` +
      `frame-src https://m.stripe.com/ https://m.stripe.network/ https://js.stripe.com/ 'unsafe-inline'; ` +
      `connect-src https://m.stripe.com/ https://m.stripe.network/ https://js.stripe.com/ 'unsafe-inline'; `)
  }
  for (const elementid of unusedVersions) {
    const element = doc.getElementById(elementid)
    element.parentNode.removeChild(element)
  }
  if (messageTemplate) {
    dashboard.HTML.renderTemplate(doc, null, messageTemplate, 'message-container')
    if (messageTemplate === 'succes') {
      return dashboard.Response.end(req, res, doc)
    }
  }
  if (process.env.NODE_ENV === 'development' && req.ip === '127.0.0.1') {
    req.ip = '8.8.8.8'
  }
  let defaultCountry = await global.api.user.maxmind.Country.get(req)
  let countryCode
  if (req.body) {
    countryCode = req.body.address_country
  }
  countryCode = countryCode || defaultCountry.country.iso_code
  const country = countryDivisions[countryCode]
  const states = []
  for (const code in country.divisions) {
    states.push({ code, name: country.divisions[code], object: 'state' })
  }
  states.sort(sortStates)
  dashboard.HTML.renderList(doc, states, 'state-option', 'address_state')
  if (req.body && req.body.address_state) {
    dashboard.HTML.setSelectedOptionByValue(doc, 'address_state', req.body.address_state)
  }
  dashboard.HTML.renderList(doc, countries, 'country-option', 'address_country')
  dashboard.HTML.setSelectedOptionByValue(doc, 'address_country', countryCode)
  return dashboard.Response.end(req, res, doc)
}

async function submitForm (req, res) {
  if (!req.body || req.body.refresh === 'true') {
    return renderPage(req, res)
  }
  if (!req.body.email || !req.body.email.length) {
    return renderPage(req, res, 'invalid-email')
  }
  if (!req.body.description || !req.body.description.length) {
    return renderPage(req, res, 'invalid-description')
  }
  if (!global.stripeJS) {
    if (!req.body.name || !req.body.name.length) {
      return renderPage(req, res, 'invalid-name')
    }
    if (!req.body.number || !req.body.number.length) {
      return renderPage(req, res, 'invalid-number')
    }
    if (!req.body.cvc || !req.body.cvc.length) {
      return renderPage(req, res, 'invalid-cvc')
    }
    if (!req.body.exp_month || !req.body.exp_month.length) {
      return renderPage(req, res, 'invalid-exp_month')
    }
    if (!req.body.exp_year || !req.body.exp_year.length) {
      return renderPage(req, res, 'invalid-exp_year')
    }
  } else if (global.stripeJS === 2 || global.stripeJS === 3) {
    if (!req.body.token || !req.body.token.length) {
      return renderPage(req, res, 'invalid-token')
    }
  }
  try {
    req.query = req.query || {}
    req.query.accountid = req.account.accountid
    await global.api.user.subscriptions.CreateCustomerWithCard.post(req)
    if (req.success) {
      if (req.query.returnURL) {
        return dashboard.Response.redirect(req, res, returnURL)
      }
      return dashboard.Response.redirect(req, res, '/account/subscriptions/billing-profiles')
    }
    return dashboard.Response.redirect(req, res, '/account/authorize')
  } catch (error) {
    return renderPage(req, res, 'unknown-error')
  }
}

function sortStates (a, b) {
  return a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1
}
