/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../test-helper.js')

describe(`/administrator/subscriptions/forgive-invoice`, async () => {
  describe('ForgiveInvoice#BEFORE', () => {
    it('should reject invalid invoiceid', async () => {
      const administrator = await TestHelper.createAdministrator()
      const req = TestHelper.createRequest(`/administrator/subscriptions/forgive-invoice?invoiceid=invalid`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      let errorMessage
      try {
        await req.route.api.before(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-invoiceid')
    })

    it('should reject paid invoice', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const plan1 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, amount: 1000, trial_period_days: 0 })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      await TestHelper.createSubscription(user, plan1.id)
      const invoiceid = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, null)
      const req = TestHelper.createRequest(`/administrator/subscriptions/forgive-invoice?invoiceid=${invoiceid}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      let errorMessage
      try {
        await req.route.api.before(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-invoice')
    })

    it('should reject forgiven invoice', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const plan1 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, amount: 1000, trial_period_days: 0 })
      const plan2 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, amount: 2000, trial_period_days: 0 })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      await TestHelper.createSubscription(user, plan1.id)
      const invoiceid1 = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, null)
      await TestHelper.changeSubscriptionWithoutPaying(user, plan2.id)
      const invoiceid2 = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, invoiceid1)
      await TestHelper.forgiveInvoice(administrator, invoiceid2)
      const req = TestHelper.createRequest(`/administrator/subscriptions/forgive-invoice?invoiceid=${invoiceid2}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      let errorMessage
      try {
        await req.route.api.before(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-invoice')
    })

    it('should bind invoice to req', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const plan1 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, amount: 1000, trial_period_days: 0 })
      const plan2 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, amount: 2000, trial_period_days: 0 })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      await TestHelper.createSubscription(user, plan1.id)
      const invoiceid1 = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, null)
      await TestHelper.changeSubscriptionWithoutPaying(user, plan2.id)
      const invoiceid2 = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, invoiceid1)
      const req = TestHelper.createRequest(`/administrator/subscriptions/forgive-invoice?invoiceid=${invoiceid2}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      await req.route.api.before(req)
      assert.strictEqual(req.data.invoice.id, invoiceid2)
    })
  })

  describe('ForgiveInvoice#GET', () => {
    it('should present the form', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const plan1 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, amount: 1000, trial_period_days: 0 })
      const plan2 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, amount: 2000, trial_period_days: 0 })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      await TestHelper.createSubscription(user, plan1.id)
      const invoiceid1 = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, null)
      await TestHelper.changeSubscriptionWithoutPaying(user, plan2.id)
      const invoiceid2 = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, invoiceid1)
      const req = TestHelper.createRequest(`/administrator/subscriptions/forgive-invoice?invoiceid=${invoiceid2}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        assert.strictEqual(doc.getElementById('submit-form').tag, 'form')
        assert.strictEqual(doc.getElementById('submit-button').tag, 'button')
      }
      return req.route.api.get(req, res)
    })

    it('should present the invoice table', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const plan1 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, amount: 1000, trial_period_days: 0 })
      const plan2 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, amount: 2000, trial_period_days: 0 })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      await TestHelper.createSubscription(user, plan1.id)
      const invoiceid1 = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, null)
      await TestHelper.changeSubscriptionWithoutPaying(user, plan2.id)
      const invoiceid2 = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, invoiceid1)
      const req = TestHelper.createRequest(`/administrator/subscriptions/forgive-invoice?invoiceid=${invoiceid2}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const row = doc.getElementById(invoiceid2)
        assert.strictEqual(row.tag, 'tr')
      }
      return req.route.api.get(req, res)
    })
  })

  describe('ForgiveInvoice#POST', () => {
    it('should apply after authorization', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const plan1 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, amount: 1000, trial_period_days: 0 })
      const plan2 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, amount: 2000, trial_period_days: 0 })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      await TestHelper.createSubscription(user, plan1.id)
      const invoiceid1 = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, null)
      await TestHelper.changeSubscriptionWithoutPaying(user, plan2.id)
      const invoiceid2 = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, invoiceid1)
      const req = TestHelper.createRequest(`/administrator/subscriptions/forgive-invoice?invoiceid=${invoiceid2}`, 'POST')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      req.body = {}
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.administratorSession = req.session = await TestHelper.unlockSession(administrator)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.get(req, res2)
      }
      return req.route.api.post(req, res)
    })
  })
})
