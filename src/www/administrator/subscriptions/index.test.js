/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../test-helper.js')

describe(`/administrator/subscriptions`, async () => {
  describe('Index#BEFORE', () => {
    it('should bind plans to req', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      await TestHelper.createPlan(administrator, { productid: product.id, published: true, amount: 1000, trial_period_days: 0 })
      const req = TestHelper.createRequest(`/administrator/subscriptions`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      await req.route.api.before(req)
      assert.strictEqual(req.data.plans[0].id, administrator.plan.id)
    })

    it('should bind subscriptions to req', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      await TestHelper.createPlan(administrator, { productid: product.id, published: true, amount: 1000, trial_period_days: 0 })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      await TestHelper.createSubscription(user, administrator.plan.id)
      const req = TestHelper.createRequest(`/administrator/subscriptions`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      await req.route.api.before(req)
      assert.strictEqual(req.data.subscriptions[0].id, user.subscription.id)
    })

    it('should bind coupons to req', async () => {
      const administrator = await TestHelper.createAdministrator()
      await TestHelper.createCoupon(administrator, { published: true, percent_off: 25, duration: 'repeating', duration_in_months: 3 })
      const req = TestHelper.createRequest(`/administrator/subscriptions`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      await req.route.api.before(req)
      assert.strictEqual(req.data.coupons[0].id, administrator.coupon.id)
    })
  })

  describe('Index#GET', () => {
    it('should have row for each plan', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const plan1 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, amount: 1000, trial_period_days: 0 })
      const plan2 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, amount: 2000, trial_period_days: 0 })
      const req = TestHelper.createRequest('/administrator/subscriptions', 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const plan1Row = doc.getElementById(plan1.id)
        const plan2Row = doc.getElementById(plan2.id)
        assert.strictEqual(plan1Row.tag, 'tr')
        assert.strictEqual(plan2Row.tag, 'tr')
      }
      return req.route.api.get(req, res)
    })

    it('should have row for each coupon', async () => {
      const administrator = await TestHelper.createAdministrator()
      await TestHelper.createCoupon(administrator, { published: true, percent_off: 25, duration: 'repeating', duration_in_months: 3 })
      const coupon1 = administrator.coupon
      await TestHelper.createCoupon(administrator, { published: true, percent_off: 25, duration: 'repeating', duration_in_months: 3 })
      const coupon2 = administrator.coupon
      const req = TestHelper.createRequest('/administrator/subscriptions', 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const coupon1Row = doc.getElementById(coupon1.id)
        const coupon2Row = doc.getElementById(coupon2.id)
        assert.strictEqual(coupon1Row.tag, 'tr')
        assert.strictEqual(coupon2Row.tag, 'tr')
      }
      return req.route.api.get(req, res)
    })

    it('should have row for each subscription', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const plan1 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, amount: 1000, trial_period_days: 0 })
      const plan2 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, amount: 2000, trial_period_days: 0 })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      const subscription1 = await TestHelper.createSubscription(user, plan1.id)
      const user2 = await TestHelper.createUser()
      await TestHelper.createCustomer(user2)
      await TestHelper.createCard(user2)
      const subscription2 = await TestHelper.createSubscription(user2, plan2.id)
      const req = TestHelper.createRequest('/administrator/subscriptions', 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const subscription1Row = doc.getElementById(subscription1.id)
        const subscription2Row = doc.getElementById(subscription2.id)
        assert.strictEqual(subscription1Row.tag, 'tr')
        assert.strictEqual(subscription2Row.tag, 'tr')
      }
      return req.route.api.get(req, res)
    })
  })
})
