const dashboard = require('@userappstore/dashboard')
const stripe = require('stripe')()
const stripeCache = require('../../../stripe-cache.js')

// The creation of objects like charges and invoices that happen
// without user actions are indexed as this webhook is notifed.  All
// other types of data are indexed as created by the user.
global.subscriptionWebhooksInProgress = 0

module.exports = {
  auth: false,
  template: false,
  post: async (req, res) => {
    global.subscriptionWebhooksInProgress++
    if (!global.redisClient || global.testEnded) {
      res.statusCode = 200
      endWebhook()
      return res.end()
    }
    let stripeEvent
    try {
      stripeEvent = stripe.webhooks.constructEvent(req.bodyRaw, req.headers['stripe-signature'], process.env.SUBSCRIPTIONS_ENDPOINT_SECRET)
    } catch (error) {
      res.statusCode = 500
      endWebhook()
      return res.end()
    }
    if (!stripeEvent) {
      res.statusCode = 500
      endWebhook()
      return res.end()
    }
    if (global.testNumber && stripeEvent.data && stripeEvent.data.object && stripeEvent.data.object.metadata && stripeEvent.data.object.metadata.accountid) {
      const accountid = stripeEvent.data.object.metadata.accountid
      const exists = await global.redisClient.hgetallAsync(accountid)
      if (!exists) {
        res.statusCode = 200
        endWebhook()
        return res.end()
      }
      let appid = stripeEvent.data.object.metadata.appid
      if (appid && appid !== global.appid) {
        res.statusCode = 200
        endWebhook()
        return res.end()
      }
    }
    let accountid, invoice, charge, customerid, subscriptionid, planid, productid, cardid
    const add = []
    const remove = []
    switch (stripeEvent.type) {
      case 'invoice.created':
        invoice = stripeEvent.data.object
        customerid = invoice.customer
        subscriptionid = invoice.subscription || invoice.lines.data[0].subscription
        accountid = await global.redisClient.hgetAsync(`map:accountid:customerid`, customerid)
        planid = invoice.lines.data[0].plan.id
        productid = invoice.lines.data[0].plan.product
        const invoiceNow = await stripe.invoices.update(invoice.id, {
          metadata: {
            appid: req.appid,
            accountid: accountid
          }
        }, req.stripeKey)
        await stripeCache.update(invoiceNow, req.stripeKey)
        add.push({ index: `${req.appid}:invoices`, value: invoice.id })
        add.push({ index: `${req.appid}:customer:invoices:${customerid}`, value: invoice.id })
        add.push({ index: `${req.appid}:plan:invoices:${planid}`, value: invoice.id })
        add.push({ index: `${req.appid}:product:invoices:${productid}`, value: invoice.id })
        add.push({ index: `${req.appid}:account:plan:invoices:${planid}:${accountid}`, value: invoice.id })
        add.push({ index: `${req.appid}:account:product:invoices:${productid}:${accountid}`, value: invoice.id })
        add.push({ index: `${req.appid}:subscription:invoices:${subscriptionid}`, value: invoice.id })
        add.push({ index: `${req.appid}:account:invoices:${accountid}`, value: invoice.id })
        break
      case 'charge.succeeded':
        charge = stripeEvent.data.object
        cardid = charge.source.id
        if (!charge.invoice) {
          endWebhook()
          res.statusCode = 200
          return res.end()
        }
        invoice = await stripe.invoices.retrieve(charge.invoice, req.stripeKey)
        await stripeCache.update(invoice)
        await stripeCache.update(charge)
        customerid = charge.customer
        subscriptionid = invoice.subscription || invoice.lines.data[0].subscription
        accountid = await global.redisClient.hgetAsync(`map:accountid:customerid`, customerid)
        if (!accountid) {
          endWebhook()
          res.statusCode = 200
          return res.end()
        }
        if (!charge.metadata.appid) {
          charge = await stripe.charges.update(charge.id, {
            metadata: {
              appid: req.appid,
              accountid: accountid
            }
          }, req.stripeKey)
        }
        planid = invoice.lines.data[0].plan.id
        productid = invoice.lines.data[0].plan.product
        add.push({ index: `${req.appid}:charges`, value: charge.id })
        add.push({ index: `${req.appid}:customer:charges:${customerid}`, value: charge.id })
        add.push({ index: `${req.appid}:subscription:charges:${subscriptionid}`, value: charge.id })
        add.push({ index: `${req.appid}:subscription:cards:${subscriptionid}`, value: cardid })
        add.push({ index: `${req.appid}:product:charges:${productid}`, value: charge.id })
        add.push({ index: `${req.appid}:product:cards:${productid}`, value: cardid })
        add.push({ index: `${req.appid}:plan:cards:${planid}`, value: cardid })
        add.push({ index: `${req.appid}:plan:charges:${planid}`, value: charge.id })
        add.push({ index: `${req.appid}:account:product:charges:${productid}:${accountid}`, value: charge.id })
        add.push({ index: `${req.appid}:account:product:cards:${productid}:${accountid}`, value: cardid })
        add.push({ index: `${req.appid}:account:plan:cards:${planid}:${accountid}`, value: cardid })
        add.push({ index: `${req.appid}:account:plan:charges:${planid}:${accountid}`, value: charge.id })
        add.push({ index: `${req.appid}:card:invoices:${cardid}`, value: invoice.id })
        add.push({ index: `${req.appid}:card:charges:${cardid}`, value: charge.id })
        add.push({ index: `${req.appid}:card:products:${cardid}`, value: productid })
        add.push({ index: `${req.appid}:card:plans:${cardid}`, value: planid })
        add.push({ index: `${req.appid}:card:subscriptions:${cardid}`, value: subscriptionid })
        add.push({ index: `${req.appid}:account:charges:${accountid}`, value: charge.id })
        break
      case 'charge.refunded':
        charge = stripeEvent.data.object
        const refund = charge.refunds.data[0]
        cardid = charge.source.id
        invoice = await stripe.invoices.retrieve(charge.invoice, req.stripeKey)
        customerid = charge.customer
        subscriptionid = invoice.subscription || invoice.lines.data[0].subscription
        accountid = invoice.metadata.accountid
        planid = invoice.lines.data[0].plan.id
        productid = invoice.lines.data[0].plan.product
        await stripeCache.update(charge)
        await stripeCache.update(refund)
        await stripeCache.update(invoice)
        add.push({ index: `${req.appid}:refunds`, value: refund.id })
        add.push({ index: `${req.appid}:customer:refunds:${customerid}`, value: refund.id })
        add.push({ index: `${req.appid}:plan:refunds:${planid}`, value: refund.id })
        add.push({ index: `${req.appid}:product:refunds:${productid}`, value: refund.id })
        add.push({ index: `${req.appid}:account:plan:refunds:${planid}`, value: refund.id })
        add.push({ index: `${req.appid}:account:product:refunds:${productid}`, value: refund.id })
        add.push({ index: `${req.appid}:subscription:refunds:${subscriptionid}`, value: refund.id })
        add.push({ index: `${req.appid}:card:refunds:${cardid}`, value: refund.id })
        add.push({ index: `${req.appid}:account:refunds:${accountid}`, value: refund.id })
        break
      case 'charge.dispute.created':
        const dispute = stripeEvent.data.object
        charge = await stripe.charges.retrieve(dispute.charge, req.stripeKey)
        cardid = charge.source.id
        invoice = await stripe.invoices.retrieve(charge.invoice, req.stripeKey)
        const disputeNow = await stripe.disputes.update(dispute.id, { metadata: { appid: req.appid } }, req.stripeKey)
        await stripeCache.update(disputeNow, req.stripeKey)
        await stripeCache.update(charge)
        await stripeCache.update(invoice)
        customerid = charge.customer
        subscriptionid = invoice.subscription || invoice.lines.data[0].subscription
        accountid = invoice.metadata.accountid
        planid = invoice.lines.data[0].plan.id
        productid = invoice.lines.data[0].plan.product
        add.push({ index: `${req.appid}:disputes`, value: dispute.id })
        add.push({ index: `${req.appid}:customer:disputes:${customerid}`, value: dispute.id })
        add.push({ index: `${req.appid}:plan:disputes:${planid}`, value: dispute.id })
        add.push({ index: `${req.appid}:product:disputes:${productid}`, value: dispute.id })
        add.push({ index: `${req.appid}:account:plan:disputes:${planid}:${accountid}`, value: dispute.id })
        add.push({ index: `${req.appid}:account:product:disputes:${productid}:${accountid}`, value: dispute.id })
        add.push({ index: `${req.appid}:subscription:disputes:${subscriptionid}`, value: dispute.id })
        add.push({ index: `${req.appid}:card:disputes:${cardid}`, dispute: refund.id })
        add.push({ index: `${req.appid}:account:invoices:${accountid}`, value: dispute.id })
        break
      case 'payout.created':
        const payout = stripeEvent.data.object
        await stripeCache.update(payout)
        add.push({ index: `${req.appid}:payouts`, value: payout.id })
        add.push({ index: `${req.appid}:account:payouts:${accountid}`, value: payout.id })
        break
      case 'customer.subscription.deleted':
        const subscription = stripeEvent.data.object
        customerid = subscription.customer
        planid = subscription.plan.id
        productid = subscription.plan.product
        accountid = subscription.metadata.accountid
        remove.push({ index: `${req.appid}:subscriptions`, value: subscription.id })
        remove.push({ index: `${req.appid}:customer:subscriptions:${customerid}`, value: subscription.id })
        remove.push({ index: `${req.appid}:plan:subscriptions:${planid}`, value: subscription.id })
        remove.push({ index: `${req.appid}:product:subscriptions:${productid}`, value: subscription.id })
        remove.push({ index: `${req.appid}:account:subscriptions:${accountid}`, value: subscription.id })
        // TODO: how about tests where we confirm all the data is deleted successfully
        break
      default:
        if (!stripeEvent.data || !stripeEvent.data.object || !stripeEvent.data.object.id) {
          endWebhook()
          res.statusCode = 200
          return res.end()
        }
        if (stripeEvent.type.endsWith('.updated')) {
          await stripeCache.update(stripeEvent.data.object)
          res.statusCode = 200
          endWebhook()
          return res.end()
        } else if (stripeEvent.type.endsWith('.deleted')) {
          await stripeCache.delete(stripeEvent.data.object.id)
          res.statusCode = 200
          endWebhook()
          return res.end()
        }
        endWebhook()
        res.statusCode = 200
        return res.end()
    }
    if (add.length) {
      for (const item of add) {
        if (!global.redisClient || global.testEnded) {
          res.statusCode = 200
          endWebhook()
          return res.end()
        }
        await dashboard.RedisList.add(item.index, item.value)
      }
    }
    if (remove.length) {
      for (const item of remove) {
        if (!global.redisClient || global.testEnded) {
          endWebhook()
          res.statusCode = 200
          return res.end()
        }
        await dashboard.RedisList.remove(item.index, item.value)
      }
    }
    endWebhook()
    res.statusCode = 200
    return res.end()
  }
}

function endWebhook() {
  return setTimeout(reduceCounter, 200)
}

function reduceCounter() {
  global.subscriptionWebhooksInProgress--
}
