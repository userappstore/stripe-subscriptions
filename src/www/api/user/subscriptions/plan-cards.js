const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.planid) {
      throw new Error('invalid-planid')
    }
    let cardids
    if (req.query.all) {
      cardids = await dashboard.RedisList.listAll(`${req.appid}:account:plan:cards:${req.query.planid}:${req.account.accountid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      cardids = await dashboard.RedisList.list(`${req.appid}:account:plan:cards:${req.query.planid}:${req.account.accountid}`, offset)
    }
    if (!cardids || !cardids.length) {
      return null
    }
    const cards = []
    for (const cardid of cardids) {
      req.query.cardid = cardid
      const card = await global.api.user.subscriptions.Card.get(req)
      cards.push(card)
    }
    return cards
  }
}
