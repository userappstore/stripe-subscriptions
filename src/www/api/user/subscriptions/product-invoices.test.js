/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/user/subscriptions/product-invoices', () => {
  describe('ProductInvoices#GET', () => {
    it('should limit invoices to one page', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const plan1 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 10000 })
      const plan2 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 20000 })
      const plan3 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 30000 })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      await TestHelper.createSubscription(user, plan1.id)
      await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, null)
      await TestHelper.createSubscription(user, plan2.id)
      const invoiceid2 = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, null)
      await TestHelper.createSubscription(user, plan3.id)
      const invoiceid3 = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, null)
      const req = TestHelper.createRequest(`/api/user/subscriptions/product-invoices?productid=${product.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      const invoices = await req.route.api.get(req)
      assert.strictEqual(invoices.length, global.pageSize)
      assert.strictEqual(invoices[0].id, invoiceid3)
      assert.strictEqual(invoices[1].id, invoiceid2)
    })

    it('should enforce page size', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 10000 })
      global.pageSize = 3
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createSubscription(user, administrator.plan.id)
        await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, null)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/product-invoices?productid=${product.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      const invoicesNow = await req.route.api.get(req)
      assert.strictEqual(invoicesNow.length, global.pageSize)
    })

    it('should enforce specified offset', async () => {
      const offset = 1
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 10000 })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      const invoices = []
      for (let i = 0, len = offset + global.pageSize + 1; i < len; i++) {
        await TestHelper.createSubscription(user, administrator.plan.id)
        const invoiceid = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, null)
        await TestHelper.cancelSubscription(user, user.subscription.id)
        invoices.unshift(invoiceid)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/product-invoices?productid=${product.id}&offset=${offset}`, 'GET')
      req.account = user.account
      req.session = user.session
      const invoicesNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(invoicesNow[i].id, invoices[offset + i])
      }
    })

    it('should return all records', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 10000 })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      const invoices = []
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createSubscription(user, administrator.plan.id)
        const invoiceid = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, null)
        await TestHelper.cancelSubscription(user, user.subscription.id)
        invoices.unshift(invoiceid)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/product-invoices?productid=${product.id}&all=true`, 'GET')
      req.account = user.account
      req.session = user.session
      const invoicesNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(invoicesNow[i].id, invoices[i])
      }
    })
  })
})
