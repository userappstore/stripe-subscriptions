const dashboard = require('@userappstore/dashboard')
const stripe = require('stripe')()
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.subscriptionid) {
      throw new Error('invalid-subscriptionid')
    }
    const subscription = await global.api.user.subscriptions.Subscription.get(req)
    if (!subscription) {
      throw new Error('invalid-subscriptionid')
    }
    if (!req.body || !req.body.tax_percent) {
      throw new Error('invalid-tax_percent')
    }
    try {
      const percent = parseInt(req.body.tax_percent, 10)
      if (percent < 1 || percent.toString() !== req.body.tax_percent) {
        throw new Error('invalid-tax_percent')
      }
    } catch (error) {
      throw new Error('invalid-tax_percent')
    }
    if (subscription.tax_percent === req.body.tax_percent) {
      throw new Error('invalid-tax_percent')
    }
  },
  patch: async (req) => {
    const updateInfo = {
      tax_percent: req.body.tax_percent
    }
    try {
      const subscription = await stripe.subscriptions.update(req.query.subscriptionid, updateInfo, req.stripeKey)
      await stripeCache.update(subscription, req.stripeKey)
      req.success = true
      return subscription
    } catch (error) {
      throw new Error('unknown-error')
    }
  }
}
