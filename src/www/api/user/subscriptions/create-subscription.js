const dashboard = require('@userappstore/dashboard')
const stripe = require('stripe')()
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.customerid) {
      throw new Error('invalid-customerid')
    }
    if (!req.body || !req.body.planid) {
      throw new Error('invalid-planid')
    }
    const customer = await global.api.user.subscriptions.Customer.get(req)
    req.query.planid = req.body.planid
    const plan = await global.api.user.subscriptions.PublishedPlan.get(req)
    if (!customer.default_source && plan.amount) {
      throw new Error('invalid-cardid')
    }
    req.data = { plan }
  },
  post: async (req) => {
    if (req.body.quantity) {
      try {
        const quantity = parseInt(req.body.quantity, 10)
        if (quantity < 1 || quantity.toString() !== req.body.quantity) {
          throw new Error('invalid-quantity')
        }
      } catch (error) {
        throw new Error('invalid-quantity')
      }
    }
    if (req.body.tax_percent) {
      try {
        const percent = parseInt(req.body.tax_percent, 10)
        if (percent < 1 || percent.toString() !== req.body.tax_percent) {
          throw new Error('invalid-tax_percent')
        }
      } catch (error) {
        throw new Error('invalid-tax_percent')
      }
    }
    const subscriptionInfo = {
      customer: req.query.customerid,
      items: [{
        plan: req.body.planid
      }],
      metadata: {
        appid: req.appid
      },
      tax_percent: req.body.tax_percent || 0
    }
    if (req.body.quantity > 1) {
      subscriptionInfo.quantity = req.body.quantity
    }
    try {
      const subscription = await stripe.subscriptions.create(subscriptionInfo, req.stripeKey)
      await stripeCache.update(subscription)
      await dashboard.RedisList.add(`${req.appid}:subscriptions`, subscription.id)
      await dashboard.RedisList.add(`${req.appid}:account:subscriptions:${req.account.accountid}`, subscription.id)
      await dashboard.RedisList.add(`${req.appid}:customer:subscriptions:${req.query.customerid}`, subscription.id)
      await dashboard.RedisList.add(`${req.appid}:account:plan:subscriptions:${req.query.planid}:${req.account.accountid}`, subscription.id)
      await dashboard.RedisList.add(`${req.appid}:account:product:subscriptions:${req.data.plan.product}:${req.account.accountid}`, subscription.id)
      await dashboard.RedisList.add(`${req.appid}:account:plan:customers:${req.query.planid}:${req.account.accountid}`, req.query.customerid)
      await dashboard.RedisList.add(`${req.appid}:account:product:customers:${req.data.plan.product}:${req.account.accountid}`, req.query.customerid)
      await dashboard.RedisList.add(`${req.appid}:plan:subscriptions:${req.query.planid}`, subscription.id)
      await dashboard.RedisList.add(`${req.appid}:product:subscriptions:${req.data.plan.product}`, subscription.id)
      await dashboard.RedisList.add(`${req.appid}:plan:customers:${req.query.planid}`, req.query.customerid)
      await dashboard.RedisList.add(`${req.appid}:product:customers:${req.data.plan.product}`, req.query.customerid)
      req.success = true
      return subscription
    } catch (error) {
      throw new Error('unknown-error')
    }
  }
}
