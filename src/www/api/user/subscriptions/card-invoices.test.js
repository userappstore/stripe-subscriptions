/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/user/subscriptions/card-invoices', () => {
  describe('CardInvoices#GET', () => {
    it('should limit card\'s invoices to one page', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      let invoiceid
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, plan.id)
        invoiceid = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, invoiceid)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/card-invoices?cardid=${user.card.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      const invoicesNow = await req.route.api.get(req)
      assert.strictEqual(invoicesNow.length, global.pageSize)
    })

    it('should enforce page size', async () => {
      global.pageSize = 3
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      let invoiceid
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, plan.id)
        invoiceid = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, invoiceid)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/card-invoices?cardid=${user.card.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      const invoicesNow = await req.route.api.get(req)
      assert.strictEqual(invoicesNow.length, global.pageSize)
    })

    it('should enforce specified offset', async () => {
      const offset = 1
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      const invoices = []
      let invoiceid
      for (let i = 0, len = global.pageSize + offset + 1; i < len; i++) {
        const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, plan.id)
        invoiceid = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, invoiceid)
        invoices.unshift(invoiceid)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/card-invoices?cardid=${user.card.id}&offset=${offset}`, 'GET')
      req.account = user.account
      req.session = user.session
      const invoicesNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(invoicesNow[i].id, invoices[offset + i])
      }
    })

    it('should return all records', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      const invoices = []
      let invoiceid
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, plan.id)
        invoiceid = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, invoiceid)
        invoices.unshift(invoiceid)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/card-invoices?cardid=${user.card.id}&all=true`, 'GET')
      req.account = user.account
      req.session = user.session
      const invoicesNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(invoicesNow[i].id, invoices[i])
      }
    })
  })
})
