const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.cardid) {
      throw new Error('invalid-cardid')
    }
    const card = await global.api.user.subscriptions.Card.get(req)
    if (!card) {
      throw new Error('invalid-cardid')
    }
    let cardids
    if (req.query.all) {
      cardids = await dashboard.RedisList.listAll(`${req.appid}:card:charges:${req.query.cardid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      cardids = await dashboard.RedisList.list(`${req.appid}:card:charges:${req.query.cardid}`, offset)
    }
    if (!cardids || !cardids.length) {
      return null
    }
    const items = []
    for (const chargeid of cardids) {
      req.query.chargeid = chargeid
      const charge = await global.api.user.subscriptions.Charge.get(req)
      items.push(charge)
    }
    return items
  }
}
