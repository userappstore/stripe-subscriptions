/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/user/subscriptions/subscription-invoices', () => {
  describe('SubscriptionInvoices#GET', () => {
    it('should limit invoices on subscription to one page', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 10000, interval: 'day' })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      await TestHelper.createSubscription(user, plan.id)
      let lastInvoice = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, null)
      const intervals = ['week', 'month', 'year']
      const amounts = [20000, 30000, 40000]
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const newPlan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: amounts[i], interval: intervals[i] })
        await TestHelper.changeSubscription(user, newPlan.id)
        lastInvoice = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, lastInvoice)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/subscription-invoices?subscriptionid=${user.subscription.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      const invoicesNow = await req.route.api.get(req)
      assert.strictEqual(invoicesNow.length, global.pageSize)
    })

    it('should enforce page size', async () => {
      global.pageSize = 3
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 10000, interval: 'day' })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      await TestHelper.createSubscription(user, plan.id)
      let lastInvoice = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, null)
      const intervals = ['week', 'month', 'year', 'day']
      const amounts = [20000, 30000, 40000, 50000]
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const newPlan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: amounts[i], interval: intervals[i] })
        await TestHelper.changeSubscription(user, newPlan.id)
        lastInvoice = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, lastInvoice)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/subscription-invoices?subscriptionid=${user.subscription.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      const invoicesNow = await req.route.api.get(req)
      assert.strictEqual(invoicesNow.length, global.pageSize)
    })

    it('should enforce specified offset', async () => {
      const offset = 1
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 10000, interval: 'day' })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      await TestHelper.createSubscription(user, plan.id)
      let lastInvoice = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, null)
      const intervals = ['week', 'month', 'year', 'day']
      const amounts = [20000, 30000, 40000, 50000]
      const invoices = []
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const newPlan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: amounts[i], interval: intervals[i] })
        await TestHelper.changeSubscription(user, newPlan.id)
        lastInvoice = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, lastInvoice)
        invoices.unshift(lastInvoice)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/subscription-invoices?subscriptionid=${user.subscription.id}&offset=${offset}`, 'GET')
      req.account = user.account
      req.session = user.session
      const invoicesNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(invoicesNow[i].id, invoices[offset + i])
      }
    })

    it('should return all records', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 10000, interval: 'day' })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      await TestHelper.createSubscription(user, plan.id)
      let lastInvoice = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, null)
      const intervals = ['week', 'month', 'year', 'day']
      const amounts = [20000, 30000, 40000, 50000]
      const invoices = []
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const newPlan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: amounts[i], interval: intervals[i] })
        await TestHelper.changeSubscription(user, newPlan.id)
        lastInvoice = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, lastInvoice)
        invoices.unshift(lastInvoice)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/subscription-invoices?subscriptionid=${user.subscription.id}&all=true`, 'GET')
      req.account = user.account
      req.session = user.session
      const invoicesNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        assert.strictEqual(invoicesNow[i].id, invoices[i])
      }
    })
  })
})
