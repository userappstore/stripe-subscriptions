const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.subscriptionid) {
      throw new Error('invalid-subscriptionid')
    }
    const subscription = await global.api.user.subscriptions.Subscription.get(req)
    if (!subscription) {
      throw new Error('invalid-subscriptionid')
    }
    let refundids
    if (req.query.all) {
      refundids = await dashboard.RedisList.listAll(`${req.appid}:subscription:refunds:${req.query.subscriptionid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      refundids = await dashboard.RedisList.list(`${req.appid}:subscription:refunds:${req.query.subscriptionid}`, offset)
    }
    if (!refundids || !refundids.length) {
      return null
    }
    const items = []
    for (const refundid of refundids) {
      req.query.refundid = refundid
      const item = await global.api.user.subscriptions.Refund.get(req)
      items.push(item)
    }
    return items
  }
}
