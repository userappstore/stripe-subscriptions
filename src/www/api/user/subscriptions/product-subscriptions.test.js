/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/user/subscriptions/product-subscriptions', () => {
  describe('ProductSubscriptions#GET', () => {
    it('should limit subscriptions to one page', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const plan1 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 10000 })
      const plan2 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 20000 })
      const plan3 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 30000 })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      await TestHelper.createSubscription(user, plan1.id)
      await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, null)
      const subscription2 = await TestHelper.createSubscription(user, plan2.id)
      const subscription3 = await TestHelper.createSubscription(user, plan3.id)
      const req = TestHelper.createRequest(`/api/user/subscriptions/product-subscriptions?productid=${product.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      const subscriptions = await req.route.api.get(req)
      assert.strictEqual(subscriptions.length, global.pageSize)
      assert.strictEqual(subscriptions[0].id, subscription3.id)
      assert.strictEqual(subscriptions[1].id, subscription2.id)
    })

    it('should enforce page size', async () => {
      global.pageSize = 3
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 10000 })
        await TestHelper.createSubscription(user, administrator.plan.id)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/product-subscriptions?productid=${product.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      const subscriptionsNow = await req.route.api.get(req)
      assert.strictEqual(subscriptionsNow.length, global.pageSize)
    })

    it('should enforce specified offset', async () => {
      const offset = 1
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      const subscriptions = []
      for (let i = 0, len = offset + global.pageSize + 1; i < len; i++) {
        await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 10000 })
        const subscription = await TestHelper.createSubscription(user, administrator.plan.id)
        subscriptions.unshift(subscription)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/product-subscriptions?productid=${product.id}&offset=${offset}`, 'GET')
      req.account = user.account
      req.session = user.session
      const subscriptionsNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(subscriptionsNow[i].id, subscriptions[offset + i].id)
      }
    })

    it('should return all records', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      const subscriptions = []
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 10000 })
        const subscription = await TestHelper.createSubscription(user, administrator.plan.id)
        subscriptions.unshift(subscription)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/product-subscriptions?productid=${product.id}&all=true`, 'GET')
      req.account = user.account
      req.session = user.session
      const subscriptionsNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(subscriptionsNow[i].id, subscriptions[i].id)
      }
    })
  })
})
