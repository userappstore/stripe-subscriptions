const dashboard = require('@userappstore/dashboard')
const stripe = require('stripe')()
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    if (req.query.accountid !== req.account.accountid) {
      throw new Error('invalid-account')
    }
    if (!req.body || !req.body.email || !req.body.email.length) {
      throw new Error('invalid-email')
    }
    if (!req.body.description || !req.body.description.length) {
      throw new Error('invalid-description')
    }
  },
  post: async (req) => {
    const customerInfo = {
      email: req.body.email,
      description: req.body.description,
      metadata: {
        appid: req.appid,
        accountid: req.account.accountid
      }
    }
    let customer
    try {
      customer = await stripe.customers.create(customerInfo, req.stripeKey)
    } catch (error) {
      throw new Error('unknown-error')
    }
    await stripeCache.update(customer)
    await dashboard.RedisObject.setProperty(req.account.accountid, `customerid`, customer.id)
    await dashboard.RedisList.add(`${req.appid}:customers`, customer.id)
    await dashboard.RedisList.add(`${req.appid}:account:customers:${req.query.accountid}`, customer.id)
    await global.redisClient.hsetAsync(`map:accountid:customerid`, customer.id, req.query.accountid)
    req.success = true
    return customer
  }
}
