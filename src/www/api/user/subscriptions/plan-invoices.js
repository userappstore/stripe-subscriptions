const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.planid) {
      throw new Error('invalid-planid')
    }
    let invoiceids
    if (req.query.all) {
      invoiceids = await dashboard.RedisList.listAll(`${req.appid}:account:plan:invoices:${req.query.planid}:${req.account.accountid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      invoiceids = await dashboard.RedisList.list(`${req.appid}:account:plan:invoices:${req.query.planid}:${req.account.accountid}`, offset)
    }
    if (!invoiceids || !invoiceids.length) {
      return null
    }
    const invoices = []
    for (const invoiceid of invoiceids) {
      req.query.invoiceid = invoiceid
      const invoice = await global.api.user.subscriptions.Invoice.get(req)
      invoices.push(invoice)
    }
    return invoices
  }
}
