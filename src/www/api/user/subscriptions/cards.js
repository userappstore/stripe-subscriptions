const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    if (req.query.accountid !== req.account.accountid) {
      throw new Error('invalid-account')
    }
    let cardids
    if (req.query.all) {
      cardids = await dashboard.RedisList.listAll(`${req.appid}:account:cards:${req.account.accountid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      cardids = await dashboard.RedisList.list(`${req.appid}:account:cards:${req.account.accountid}`, offset)
    }
    if (!cardids || !cardids.length) {
      return null
    }
    const cards = []
    for (const cardid of cardids) {
      req.query.cardid = cardid
      const card = await global.api.user.subscriptions.Card.get(req)
      cards.push(card)
    }
    return cards
  }
}
