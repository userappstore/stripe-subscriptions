/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/user/subscriptions/published-plans', () => {
  describe('PublishedProducts#GET', () => {
    it('should not require account', async () => {
      const administrator = await TestHelper.createAdministrator()
      await TestHelper.createProduct(administrator, { published: true })
      await TestHelper.createPlan(administrator, { productid: product.id, published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      const req = TestHelper.createRequest(`/api/user/subscriptions/published-plans`, 'GET')
      req.account = user.account
      req.session = user.session
      const plans = await req.route.api.get(req)
      assert.strictEqual(plans.length, 1)
    })

    it('should exclude never published plans', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      await TestHelper.createPlan(administrator, { productid: product.id })
      const plan2 = await TestHelper.createPlan(administrator, { productid: product.id, published: true })
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/subscriptions/published-plans`, 'GET')
      req.account = user.account
      req.session = user.session
      const plans = await req.route.api.get(req)
      assert.strictEqual(plans.length, 1)
      assert.strictEqual(plans[0].id, plan2.id)
    })

    it('should exclude unpublished plan', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const plan1 = await TestHelper.createPlan(administrator, { productid: product.id, published: true })
      await TestHelper.createPlan(administrator, { productid: product.id, published: true, unpublished: true })
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/subscriptions/published-plans`, 'GET')
      req.account = user.account
      req.session = user.session
      const plans = await req.route.api.get(req)
      assert.strictEqual(plans.length, 1)
      assert.strictEqual(plans[0].id, plan1.id)
    })

    it('should limit plan list to one page', async () => {
      const administrator = await TestHelper.createAdministrator()
      await TestHelper.createProduct(administrator, { published: true })
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createPlan(administrator, { productid: product.id, published: true })
      }
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/subscriptions/published-plans`, 'GET')
      req.account = user.account
      req.session = user.session
      const plans = await req.route.api.get(req)
      assert.strictEqual(plans.length, global.pageSize)
    })

    it('should enforce page size', async () => {
      global.pageSize = 3
      const administrator = await TestHelper.createAdministrator()
      await TestHelper.createProduct(administrator, { published: true })
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createPlan(administrator, { productid: product.id, published: true })
      }
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      const req = TestHelper.createRequest(`/api/user/subscriptions/published-plans`, 'GET')
      req.account = user.account
      req.session = user.session
      const plans = await req.route.api.get(req)
      assert.strictEqual(plans.length, global.pageSize)
    })

    it('should enforce specified offset', async () => {
      const offset = 1
      const administrator = await TestHelper.createAdministrator()
      await TestHelper.createProduct(administrator, { published: true })
      const plans = []
      for (let i = 0, len = offset + global.pageSize + 1; i < len; i++) {
        await TestHelper.createPlan(administrator, { productid: product.id, published: true })
        plans.unshift(administrator.plan)
      }
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      const req = TestHelper.createRequest(`/api/user/subscriptions/published-plans?offset=${offset}`, 'GET')
      req.account = user.account
      req.session = user.session
      const plansNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(plansNow[i].id, plans[offset + i].id)
      }
    })

    it('should return all records', async () => {
      const administrator = await TestHelper.createAdministrator()
      await TestHelper.createProduct(administrator, { published: true })
      const plans = []
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createPlan(administrator, { productid: product.id, published: true })
        plans.unshift(administrator.plan)
      }
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      const req = TestHelper.createRequest(`/api/user/subscriptions/published-plans?all=true`, 'GET')
      req.account = user.account
      req.session = user.session
      const plansNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(plansNow[i].id, plans[i].id)
      }
    })
  })
})
