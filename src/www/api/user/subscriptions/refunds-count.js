const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    return dashboard.RedisList.count(`${req.appid}:account:refunds:${req.account.accountid}`, req.stripeKey)
  }
}
