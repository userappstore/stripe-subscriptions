const dashboard = require('@userappstore/dashboard')
const stripe = require('stripe')()
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.customerid) {
      throw new Error('invalid-customerid')
    }
    const customer = await global.api.user.subscriptions.Customer.get(req)
    if (!customer) {
      throw new Error('invalid-customer')
    }    
    if (!global.stripeJS) {
      if (!req.body || !req.body.name || !req.body.name.length) {
        throw new Error('invalid-name')
      }
      if (!req.body.number || !req.body.number.length) {
        throw new Error('invalid-number')
      }
      if (!req.body.cvc || !req.body.cvc.length) {
        throw new Error('invalid-cvc')
      }
      if (!req.body.exp_month || !req.body.exp_month.length) {
        throw new Error('invalid-exp_month')
      }
      if (!req.body.exp_year || !req.body.exp_year.length) {
        throw new Error('invalid-exp_year')
      }
    } else if (global.stripeJS === 2 || global.stripeJS === 3) {
      if (!req.body || !req.body.token || !req.body.token.length) {
        throw new Error('invalid-token')
      }
    }
  },
  post: async (req) => {
    let cardInfo
    // no JS
    if (!global.stripeJS) {
      cardInfo = {
        metadata: {
          appid: req.appid
        },
        source: {
          object: 'card',
          name: req.body.name,
          number: req.body.number,
          cvc: req.body.cvc,
          exp_month: req.body.exp_month,
          exp_year: req.body.exp_year
        }
      }
      for (const field of ['address_line1', 'address_line2', 'address_city', 'address_state', 'address_zip', 'address_country']) {
        if (req.body[field] && req.body[field].length) {
          cardInfo.source[field] = req.body[field]
        }
      }
    } else if (global.stripeJS === 2 || global.stripeJS === 3) {
      cardInfo = {
        source: req.body.token
      }
    }
    let card
    try {
      card = await stripe.customers.createCard(req.query.customerid, cardInfo, req.stripeKey)
    } catch (error) {
      throw new Error('unknown-error')
    }
    const customerNow = await stripe.customers.update(req.query.customerid, { default_source: card.id }, req.stripeKey)
    await stripeCache.update(customerNow)
    await dashboard.RedisList.add(`${req.appid}:cards`, card.id)
    await dashboard.RedisList.add(`${req.appid}:customer:cards:${req.query.customerid}`, card.id)
    await dashboard.RedisList.add(`${req.appid}:account:cards:${req.account.accountid}`, card.id)
    await global.redisClient.hsetAsync(`map:cardid:customerid`, card.id, req.query.customerid)
    req.success = true
    return card
  }
}
