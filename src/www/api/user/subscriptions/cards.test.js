/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/user/subscriptions/cards', () => {
  describe('Cards#GET', () => {
    it('should limit cards to one page', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      const card2 = await TestHelper.createCard(user)
      const card3 = await TestHelper.createCard(user)
      const req = TestHelper.createRequest(`/api/user/subscriptions/cards?accountid=${user.account.accountid}`, 'GET')
      req.account = user.account
      req.session = user.session
      const cards = await req.route.api.get(req)
      assert.strictEqual(cards.length, global.pageSize)
      assert.strictEqual(cards[0].id, card3.id)
      assert.strictEqual(cards[1].id, card2.id)
    })

    it('should enforce page size', async () => {
      global.pageSize = 3
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createCard(user)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/cards?accountid=${user.account.accountid}`, 'GET')
      req.account = user.account
      req.session = user.session
      const cardsNow = await req.route.api.get(req)
      assert.strictEqual(cardsNow.length, global.pageSize)
    })

    it('should enforce specified offset', async () => {
      const offset = 1
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      const cards = []
      for (let i = 0, len = offset + global.pageSize + 1; i < len; i++) {
        const card = await TestHelper.createCard(user)
        cards.unshift(card)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/cards?accountid=${user.account.accountid}&offset=${offset}`, 'GET')
      req.account = user.account
      req.session = user.session
      const cardsNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(cardsNow[i].id, cards[offset + i].id)
      }
    })

    it('should return all records', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      const cards = []
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const card = await TestHelper.createCard(user)
        cards.unshift(card)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/cards?accountid=${user.account.accountid}&all=true`, 'GET')
      req.account = user.account
      req.session = user.session
      const cardsNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(cardsNow[i].id, cards[i].id)
      }
    })
  })
})
