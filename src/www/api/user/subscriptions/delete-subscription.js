const dashboard = require('@userappstore/dashboard')
const stripe = require('stripe')()
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.subscriptionid) {
      throw new Error('invalid-subscriptionid')
    }
    const subscription = await global.api.user.subscriptions.Subscription.get(req)
    if (!subscription) {
      throw new Error('invalid-subscriptionid')
    }
    if (subscription.status === 'deleted' || subscription.cancel_at_period_end) {
      throw new Error('invalid-subscription')
    }
    req.data = { subscription }
  },
  delete: async (req) => {
    const deleteOptions = {
      at_period_end: req.body && req.body.refund === 'at_period_end'
    }
    try {
      const subscription = await stripe.subscriptions.del(req.query.subscriptionid, deleteOptions, req.stripeKey)
      await dashboard.RedisList.remove(`${req.appid}:subscriptions`, req.data.subscription.id)
      await dashboard.RedisList.remove(`${req.appid}:account:subscriptions:${req.account.accountid}`, req.query.subscriptionid)
      await dashboard.RedisList.remove(`${req.appid}:plan:subscriptions:${req.data.subscription.plan.id}`, req.query.subscriptionid)
      await dashboard.RedisList.remove(`${req.appid}:product:subscriptions:${req.data.subscription.plan.product}`, req.query.subscriptionid)
      req.success = true
      await stripeCache.delete(req.query.subscriptionid)
      return subscription
    } catch (error) {
      throw new Error('unknown-error')
    }
  }
}
