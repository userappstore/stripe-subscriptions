const dashboard = require('@userappstore/dashboard')

module.exports = {
  auth: false,
  get: async (req) => {
    req.query = req.query || {}
    let planids
    if (req.query.all) {
      planids = await dashboard.RedisList.listAll(`${req.appid}:published:plans`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      planids = await dashboard.RedisList.list(`${req.appid}:published:plans`, offset)
    }
    if (!planids || !planids.length) {
      return null
    }
    const items = []
    for (const planid of planids) {
      req.query.planid = planid
      const item = await global.api.user.subscriptions.PublishedPlan.get(req)
      items.push(item)
    }
    return items
  }
}
