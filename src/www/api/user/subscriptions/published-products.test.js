/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/user/subscriptions/published-products', () => {
  describe('PublishedProducts#GET', () => {
    it('should not require account', async () => {
      const administrator = await TestHelper.createAdministrator()
      await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      const req = TestHelper.createRequest(`/api/user/subscriptions/published-products`, 'GET')
      req.account = user.account
      req.session = user.session
      const products = await req.route.api.get(req)
      assert.strictEqual(products.length, 1)
    })

    it('should exclude never published products', async () => {
      const administrator = await TestHelper.createAdministrator()
      await TestHelper.createProduct(administrator)
      const product2 = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/subscriptions/published-products`, 'GET')
      req.account = user.account
      req.session = user.session
      const products = await req.route.api.get(req)
      assert.strictEqual(products.length, 1)
      assert.strictEqual(products[0].id, product2.id)
    })

    it('should exclude unpublished product', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product1 = await TestHelper.createProduct(administrator, { published: true })
      await TestHelper.createProduct(administrator, { published: true, unpublished: true })
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/subscriptions/published-products`, 'GET')
      req.account = user.account
      req.session = user.session
      const products = await req.route.api.get(req)
      assert.strictEqual(products.length, 1)
      assert.strictEqual(products[0].id, product1.id)
    })

    it('should limit product list to one page', async () => {
      const administrator = await TestHelper.createAdministrator()
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createProduct(administrator, { published: true })
      }
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/subscriptions/published-products`, 'GET')
      req.account = user.account
      req.session = user.session
      const products = await req.route.api.get(req)
      assert.strictEqual(products.length, global.pageSize)
    })

    it('should enforce page size', async () => {
      global.pageSize = 3
      const administrator = await TestHelper.createAdministrator()
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createProduct(administrator, { published: true })
      }
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      const req = TestHelper.createRequest(`/api/user/subscriptions/published-products`, 'GET')
      req.account = user.account
      req.session = user.session
      const products = await req.route.api.get(req)
      assert.strictEqual(products.length, global.pageSize)
    })

    it('should enforce specified offset', async () => {
      const offset = 1
      const administrator = await TestHelper.createAdministrator()
      const products = []
      for (let i = 0, len = offset + global.pageSize + 1; i < len; i++) {
        await TestHelper.createProduct(administrator, { published: true })
        products.unshift(administrator.product)
      }
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      const req = TestHelper.createRequest(`/api/user/subscriptions/published-products?offset=${offset}`, 'GET')
      req.account = user.account
      req.session = user.session
      const productsNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(productsNow[i].id, products[offset + i].id)
      }
    })

    it('should return all records', async () => {
      const administrator = await TestHelper.createAdministrator()
      const products = []
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createProduct(administrator, { published: true })
        products.unshift(administrator.product)
      }
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      const req = TestHelper.createRequest(`/api/user/subscriptions/published-products?all=true`, 'GET')
      req.account = user.account
      req.session = user.session
      const productsNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(productsNow[i].id, products[i].id)
      }
    })
  })
})
