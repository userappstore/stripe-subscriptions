const stripe = require('stripe')()
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.invoiceid) {
      throw new Error('invalid-invoiceid')
    }
    if (!req.body || !req.body.cardid) {
      throw new Error('invalid-cardid')
    }
    const invoice = await global.api.user.subscriptions.Invoice.get(req)
    if (!invoice) {
      throw new Error('invalid-invoiceid')
    }
    if (invoice.forgiven || invoice.paid) {
      throw new Error('invalid-invoice')
    }
    req.query.cardid = req.body.cardid
    const card = await global.api.user.subscriptions.Card.get(req)
    if (!card) {
      throw new Error('invalid-cardid')
    }
    req.data = { invoice }
  },
  patch: async (req) => {

    try {
      const invoice = await stripe.invoices.pay(req.query.invoiceid, { source: req.body.sourceid }, req.stripeKey)
      req.success = true
      await stripeCache.update(invoice)
      return invoice
    } catch (error) {
      if (error.message.indexOf(`does not have a linked source`) === 0) {
        throw new Error('invalid-cardid')
      }
      throw new Error('unknown-error')
    }
  }
}
