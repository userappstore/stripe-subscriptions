const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.productid) {
      throw new Error('invalid-productid')
    }
    let subscriptionids
    if (req.query.all) {
      subscriptionids = await dashboard.RedisList.listAll(`${req.appid}:account:product:subscriptions:${req.query.productid}:${req.account.accountid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      subscriptionids = await dashboard.RedisList.list(`${req.appid}:account:product:subscriptions:${req.query.productid}:${req.account.accountid}`, offset)
    } 
    if (!subscriptionids || !subscriptionids.length) {
      return null
    }
    const subscriptions = []
    for (const subscriptionid of subscriptionids) {
      req.query.subscriptionid = subscriptionid
      const subscription = await global.api.user.subscriptions.Subscription.get(req)
      subscriptions.push(subscription)
    }
    return subscriptions
  }
}
