const stripe = require('stripe')()

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.subscriptionid) {
      throw new Error('invalid-subscriptionid')
    }
    const subscription = await global.api.user.subscriptions.Subscription.get(req)
    let invoice
    try {
      invoice = await stripe.invoices.retrieveUpcoming(subscription.customer, req.query.subscriptionid, req.stripeKey)
    } catch (error) {
    }
    if (!invoice) {
      throw new Error('invalid-subscriptionid')
    }
    return invoice
  }
}
