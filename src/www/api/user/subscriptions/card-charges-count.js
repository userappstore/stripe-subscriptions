const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.cardid) {
      throw new Error('invalid-cardid')
    }
    const card = await global.api.user.subscriptions.Card.get(req)
    if (!card) {
      throw new Error('invalid-card')
    }
    return dashboard.RedisList.count(`${req.appid}:card:charges:${req.query.cardid}`)
  }
}
