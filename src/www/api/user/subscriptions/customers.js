const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    if (req.account.accountid !== req.query.accountid) {
      throw new Error('invalid-account')
    }
    let customerids
    if (req.query.all) {
      customerids = await dashboard.RedisList.listAll(`${req.appid}:account:customers:${req.query.accountid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      customerids = await dashboard.RedisList.list(`${req.appid}:account:customers:${req.query.accountid}`, offset)
    }
    if (!customerids || !customerids.length) {
      return null
    }
    const items = []
    for (const customerid of customerids) {
      req.query.customerid = customerid
      const customer = await global.api.user.subscriptions.Customer.get(req)
      items.push(customer)
    }
    return items
  }
}
