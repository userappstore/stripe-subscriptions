/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/user/subscriptions/upcoming-invoices-count', async () => {
  describe('UpcominginvoicesCount#GET', () => {
    it('should count upcoming invoices', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const plan1 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, amount: 1000, trial_period_days: 0 })
      const plan2 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, amount: 2000, trial_period_days: 0 })
      const plan3 = await TestHelper.createPlan(administrator, { productid: product.id, published: true, amount: 3000, trial_period_days: 0 })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      await TestHelper.createSubscription(user, plan1.id)
      await TestHelper.createSubscription(user, plan2.id)
      await TestHelper.createSubscription(user, plan3.id)
      const req = TestHelper.createRequest(`/api/user/subscriptions/upcoming-invoices-count?customerid=${user.customer.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      const result = await req.route.api.get(req)
      assert.strictEqual(result, 3)
    })
  })
})
