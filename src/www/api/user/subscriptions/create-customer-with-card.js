module.exports = {
  lock: true,
  post: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    if (req.account.accountid !== req.query.accountid) {
      throw new Error('invalid-account')
    }
    // note the _ methods refer to the unwrapped API methods
    // allowing locks to be skipped
    const customer = await global.api.user.subscriptions.CreateCustomer._post(req)
    req.query.customerid = customer.id
    await global.api.user.subscriptions.CreateCard._post(req)
    req.success = true
    return global.api.user.subscriptions.Customer.get(req)
  }
}
