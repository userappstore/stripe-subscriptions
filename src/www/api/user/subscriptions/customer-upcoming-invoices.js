const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.customerid) {
      throw new Error('invalid-customerid')
    }
    const customer = await global.api.user.subscriptions.Customer.get(req)
    if (!customer) {
      throw new Error('invalid-customerid')
    }
    let invoiceids

    req.query.all = true
    const subscriptions = await global.api.user.subscriptions.CustomerSubscriptions.get(req)
    const invoices = []
    for (const subscription of subscriptions) {
      req.query.subscriptionid = subscription.subscriptionid
      const invoice = await global.api.user.subscriptions.UpcomingInvoice.get(req)
      invoices.push(invoice)
    }
    if (!invoices.length) {
      return null
    }
    return invoices
  }
}
