const stripe = require('stripe')()
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.chargeid) {
      throw new Error('invalid-chargeid')
    }
    const charge = await global.api.user.subscriptions.Charge.get(req)
    req.data = { charge }
  },
  post: async (req) => {
    const refundInfo = {
      charge: req.query.chargeid,
      amount: req.data.charge.amount - (req.data.charge.amount_refunded || 0),
      reason: 'requested_by_customer',
      metadata: {
        appid: req.appid
      }
    }
    try {
      const refund = await stripe.refunds.create(refundInfo, req.stripeKey)
      await stripeCache.update(refund)
      req.success = true
      return refund
    } catch (error) {
      throw new Error('unknown-error')
    }
  }
}
