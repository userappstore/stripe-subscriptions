const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    if (req.account.accountid !== req.query.accountid) {
      throw new Error('invalid-account')
    }
    return dashboard.RedisList.count(`${req.appid}:account:invoices:${req.query.accountid}`, req.stripeKey)
  }
}
