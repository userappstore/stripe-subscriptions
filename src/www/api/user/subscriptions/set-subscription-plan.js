const stripe = require('stripe')()
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.subscriptionid) {
      throw new Error('invalid-subscriptionid')
    }
    const subscription = await global.api.user.subscriptions.Subscription.get(req)
    if (!subscription) {
      throw new Error('invalid-subscriptionid')
    }
    if (!req.body || !req.body.planid) {
      throw new Error('invalid-planid')
    }
    if (subscription.plan === req.body.planid) {
      throw new Error('invalid-plan')
    }
    if (subscription.plan && subscription.plan.id && subscription.plan.id === req.body.planid) {
      throw new Error('invalid-plan')
    }
    req.query.customerid = subscription.customer
    const customer = await global.api.user.subscriptions.Customer.get(req)
    if (!customer) {
      throw new Error('invalid-customerid')
    }
    req.query.planid = req.body.planid
    const newPlan = await global.api.user.subscriptions.PublishedPlan.get(req)
    if (!newPlan.metadata.published || newPlan.metadata.unpublished) {
      throw new Error('invalid-plan')
    }
    if (newPlan.amount && !customer.default_source) {
      throw new Error('invalid-cardid')
    }
  },
  patch: async (req) => {
    const updateInfo = {
      plan: req.body.planid
    }
    try {
      const subscription = await stripe.subscriptions.update(req.query.subscriptionid, updateInfo, req.stripeKey)
      await stripeCache.update(subscription, req.stripeKey)
      req.success = true
      return subscription
    } catch (error) {
      throw new Error('unknown-error')
    }
  }
}
