const dashboard = require('@userappstore/dashboard')
const stripe = require('stripe')()
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.customerid) {
      throw new Error('invalid-subscriptionid')
    }
    const customer = await global.api.user.subscriptions.Customer.get(req)
    if (!customer) {
      throw new Error('invalid-customerid')
    }
  },
  delete: async (req) => {

    try {
      await stripe.customers.del(req.query.customerid, req.stripeKey)
      await stripeCache.delete(req.query.customerid)
      await dashboard.RedisList.remove(`${req.appid}:customers`, req.query.customerid)
      await dashboard.RedisList.remove(`${req.appid}:account:customers:${req.account.accountid}`, req.query.customerid)
      await global.redisClient.hdelAsync(`map:accountid:customerid`, req.query.customerid)
      req.success = true
    } catch (error) {
      throw new Error('unknown-error')
    }
  }
}
