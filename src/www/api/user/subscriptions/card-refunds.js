const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.cardid) {
      throw new Error('invalid-cardid')
    }
    const card = await global.api.user.subscriptions.Card.get(req)
    if (!card) {
      throw new Error('invalid-cardid')
    }
    let refundids
    if (req.query.all) {
      refundids = await dashboard.RedisList.listAll(`${req.appid}:card:refunds:${req.query.cardid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      refundids = await dashboard.RedisList.list(`${req.appid}:card:refunds:${req.query.cardid}`, offset)
    }
    if (!refundids || !refundids.length) {
      return null
    }
    const items = []
    for (const refundid of refundids) {
      req.query.refundid = refundid
      const refund = await global.api.user.subscriptions.Refund.get(req)
      items.push(refund)
    }
    return items
  }
}
