const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    if (req.query.accountid !== req.account.accountid) {
      throw new Error('invalid-account')
    }
    let chargeids
    if (req.query.all) {
      chargeids = await dashboard.RedisList.listAll(`${req.appid}:account:charges:${req.query.accountid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      chargeids = await dashboard.RedisList.list(`${req.appid}:account:charges:${req.query.accountid}`, offset)
    }
    if (!chargeids || !chargeids.length) {
      return null
    }
    const items = []
    for (const chargeid of chargeids) {
      req.query.chargeid = chargeid
      const item = await await global.api.user.subscriptions.Charge.get(req)
      items.push(item)
    }
    return items
  }
}
