/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/user/subscriptions/customer-charges', () => {
  describe('CustomerCharges#GET', () => {
    it('should limit charges on customer to one page', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, plan.id)
        await TestHelper.waitForNextItem(`customer:charges:${user.customer.id}`, null)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/customer-charges?customerid=${user.customer.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      const chargesNow = await req.route.api.get(req)
      assert.strictEqual(chargesNow.length, global.pageSize)
    })

    it('should enforce page size', async () => {
      global.pageSize = 3
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, plan.id)
        await TestHelper.waitForNextItem(`customer:charges:${user.customer.id}`, null)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/customer-charges?customerid=${user.customer.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      const chargesNow = await req.route.api.get(req)
      assert.strictEqual(chargesNow.length, global.pageSize)
    })

    it('should enforce specified offset', async () => {
      const offset = 1
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      const charges = []
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, plan.id)
        const chargeid = await TestHelper.waitForNextItem(`customer:charges:${user.customer.id}`, charges.length ? charges[0] : null)
        charges.unshift(chargeid)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/customer-charges?customerid=${user.customer.id}&offset=${offset}`, 'GET')
      req.account = user.account
      req.session = user.session
      const chargesNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(chargesNow[i].id, charges[offset + i])
      }
    })

    it('should return all records', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      const charges = []
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, plan.id)
        const chargeid = await TestHelper.waitForNextItem(`customer:charges:${user.customer.id}`, charges.length ? charges[0] : null)
        charges.unshift(chargeid)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/customer-charges?customerid=${user.customer.id}&all=true`, 'GET')
      req.account = user.account
      req.session = user.session
      const chargesNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        assert.strictEqual(chargesNow[i].id, charges[i])
      }
    })
  })
})
