const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.planid) {
      throw new Error('invalid-planid')
    }
    let subscriptionids
    if (req.query.all) {
      subscriptionids = await dashboard.RedisList.listAll(`${req.appid}:account:plan:subscriptions:${req.query.planid}:${req.account.accountid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      subscriptionids = await dashboard.RedisList.list(`${req.appid}:account:plan:subscriptions:${req.query.planid}:${req.account.accountid}`, offset)
    }
    if (!subscriptionids || !subscriptionids.length) {
      return null
    }
    const subscriptions = []
    for (const subscriptionid of subscriptionids) {
      req.query.subscriptionid = subscriptionid
      const subscription = await global.api.user.subscriptions.Subscription.get(req)
      subscriptions.push(subscription)
    }
    return subscriptions
  }
}
