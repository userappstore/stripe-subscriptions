const dashboard = require('@userappstore/dashboard')
const stripe = require('stripe')()
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.customerid) {
      throw new Error('invalid-customerid')
    }
    if (!req.body || !req.body.couponid) {
      throw new Error('invalid-couponid')
    }
    const customer = await global.api.user.subscriptions.Customer.get(req)
    if (!customer) {
      throw new Error('invalid-customer')
    }
    if (customer.discount) {
      throw new Error('invalid-customer')
    }
    const couponExists = await dashboard.RedisList.exists(`${req.appid}:coupons`, req.body.couponid)
    if (!couponExists) {
      throw new Error('invalid-couponid')
    }
    req.query.couponid = req.body.couponid
    const coupon = await global.api.user.subscriptions.PublishedCoupon.get(req)
    if (!coupon) {
      throw new Error('invalid-couponid')
    }
  },
  patch: async (req) => {
    const customerInfo = {
      coupon: req.body.couponid
    }
    try {
      const customer = await stripe.customers.update(req.query.customerid, customerInfo, req.stripeKey)
      await stripeCache.update(customer, req.stripeKey)
      req.success = true
      return customer
    } catch (error) {
      throw new Error('unknown-error')
    }
  }
}
