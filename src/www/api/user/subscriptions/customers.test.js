/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/user/subscriptions/customers', () => {
  describe('Customers#GET', () => {
    it('should limit customers on card to one page', async () => {
      const user = await TestHelper.createUser()
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createCustomer(user)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/customers?accountid=${user.account.accountid}`, 'GET')
      req.account = user.account
      req.session = user.session
      const customers = await req.route.api.get(req)
      assert.strictEqual(customers.length, global.pageSize)
    })

    it('should enforce page size', async () => {
      global.pageSize = 3
      const user = await TestHelper.createUser()
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createCustomer(user)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/customers?accountid=${user.account.accountid}`, 'GET')
      req.account = user.account
      req.session = user.session
      const customers = await req.route.api.get(req)
      assert.strictEqual(customers.length, global.pageSize)
    })

    it('should enforce specified offset', async () => {
      const offset = 1
      const customers = []
      for (let i = 0, len = offset + global.pageSize + 1; i < len; i++) {
        await TestHelper.createCustomer(user)
        customers.unshift(user.customer)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/customers?accountid=${user.account.accountid}&offset=${offset}`, 'GET')
      req.account = user.account
      req.session = user.session
      const customersNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(customersNow[i].id, customers[offset + i].id)
      }
    })

    it('should return all records', async () => {
      const user = await TestHelper.createUser()
      const customers = []
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createCustomer(user)
        customers.unshift(user.customer)
      }
      const req = TestHelper.createRequest(`/api/user/subscriptions/customers?accountid=${user.account.accountid}&all=true`, 'GET')
      req.account = user.account
      req.session = user.session
      const customersNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        assert.strictEqual(customersNow[i].id, customers[i].id)
      }
    })
  })
})
