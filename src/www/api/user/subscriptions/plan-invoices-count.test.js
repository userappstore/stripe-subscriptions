/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/user/subscriptions/plan-invoices-count', async () => {
  describe('PlanInvoicesCount#GET', () => {
    it('should count all invoices on plan', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 10000 })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      await TestHelper.createSubscription(user, administrator.plan.id)
      const invoiceid1 = await TestHelper.waitForNextItem(`account:plan:invoices:${administrator.plan.id}:${user.account.accountid}`, null)
      await TestHelper.createSubscription(user, administrator.plan.id)
      const invoiceid2 = await TestHelper.waitForNextItem(`account:plan:invoices:${administrator.plan.id}:${user.account.accountid}`, invoiceid1)
      await TestHelper.createSubscription(user, administrator.plan.id)
      await TestHelper.waitForNextItem(`account:invoices:${user.account.accountid}`, invoiceid2)
      const req = TestHelper.createRequest(`/api/user/subscriptions/plan-invoices-count?planid=${administrator.plan.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      const result = await req.route.api.get(req)
      assert.strictEqual(result, 3)
    })
  })
})
