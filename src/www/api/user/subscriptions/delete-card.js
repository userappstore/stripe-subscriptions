const dashboard = require('@userappstore/dashboard')
const stripe = require('stripe')()
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.cardid) {
      throw new Error('invalid-subscriptionid')
    }
    const card = await global.api.user.subscriptions.Card.get(req)
    if (!card) {
      throw new Error('invalid-cardid')
    }
    req.data = { card }
  },
  delete: async (req) => {
    try {
      await stripe.customers.deleteSource(req.data.card.customer, req.query.cardid, req.stripeKey)
      await stripeCache.delete(req.query.cardid)
      await dashboard.RedisList.remove(`${req.appid}:cards`, req.query.cardid)
      await dashboard.RedisList.remove(`${req.appid}:customer:cards:${req.data.card.customer}`, req.query.cardid)
      await global.redisClient.hdelAsync(`map:cardid:customerid`, req.query.cardid)
      req.success = true
    } catch (error) {
      throw new Error('unknown-error')
    }
  }
}
