const dashboard = require('@userappstore/dashboard')
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.subscriptionid) {
      throw new Error('invalid-subscriptionid')
    }
    let invoiceids
    if (req.query.all) {
      invoiceids = await dashboard.RedisList.listAll(`${req.appid}:subscription:invoices:${req.query.subscriptionid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      invoiceids = await dashboard.RedisList.list(`${req.appid}:subscription:invoices:${req.query.subscriptionid}`, offset)
    }
    if (!invoiceids || !invoiceids.length) {
      return null
    }
    const items = []
    for (const invoiceid of invoiceids) {
      req.query.invoiceid = invoiceid
      const invoice = await global.api.administrator.subscriptions.Invoice.get(req)
      items.push(invoice)
    }
    return items
  }
}
