const dashboard = require('@userappstore/dashboard')
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.chargeid) {
      throw new Error('invalid-chargeid')
    }
    let charge
    try {
      charge = await stripeCache.retrieve(req.query.chargeid, 'charges', req.stripeKey)
    } catch (error) {
    }
    if (!charge) {
      throw new Error('invalid-chargeid')
    }
    charge.date = dashboard.Timestamp.date(charge.created)
    return charge
  }
}
