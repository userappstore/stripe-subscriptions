const stripe = require('stripe')()
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.invoiceid) {
      throw new Error('invalid-invoiceid')
    }
    const invoice = await global.api.administrator.subscriptions.Invoice.get(req)
    if (!invoice) {
      throw new Error('invalid-invoiceid')
    }
    if (invoice.forgiven || invoice.paid) {
      throw new Error('invalid-invoice')
    }
    if (!invoice.closed) {
      req.body = req.body || {}
      req.body.closed = true
    }
  },
  patch: async (req) => {
    const updateInfo = {
      forgiven: true
    }
    if (req.body.closed) {
      updateInfo.closed = true
    }
    try {
      const invoice = await stripe.invoices.update(req.query.invoiceid, updateInfo, req.stripeKey)
      await stripeCache.update(invoice, req.stripeKey)
      req.success = true
      return invoice
    } catch (error) {
      throw new Error('unknown-error')
    }
  }
}
