const dashboard = require('@userappstore/dashboard')
const stripe = require('stripe')()
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.couponid) {
      throw new Error('invalid-couponid')
    }
    const coupon = await global.api.administrator.subscriptions.Coupon.get(req)
    if (!coupon) {
      throw new Error('invalid-couponid')
    }
  },
  delete: async (req) => {
    try {
      await stripe.coupons.del(req.query.couponid, req.stripeKey)
      await dashboard.RedisList.remove(`${req.appid}:coupons`, req.query.couponid)
      await dashboard.RedisList.remove(`${req.appid}:published:coupons`, req.query.couponid)
      await dashboard.RedisList.remove(`${req.appid}:unpublished:coupons`, req.query.couponid)
      req.success = true
      await stripeCache.delete(req.query.couponid)
      return
    } catch (error) {
      throw new Error('unknown-error')
    }
  }
}
