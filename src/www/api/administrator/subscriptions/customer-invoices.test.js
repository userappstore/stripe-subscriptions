/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/administrator/subscriptions/customer-invoices', () => {
  describe('CustomerInvoices#GET', () => {
    it('should limit invoices on customer to one page', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createCard(user)
        await TestHelper.createSubscription(user, plan.id)
        await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, null)
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/customer-invoices?customerid=${user.customer.id}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const stripeAccounts = await req.route.api.get(req)
      assert.strictEqual(stripeAccounts.length, global.pageSize)
    })

    it('should enforce page size', async () => {
      global.pageSize = 3
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createCard(user)
        const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, plan.id)
        await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, null)
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/customer-invoices?customerid=${user.customer.id}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const stripeAccounts = await req.route.api.get(req)
      assert.strictEqual(stripeAccounts.length, global.pageSize)
    })

    it('should enforce specified offset', async () => {
      const offset = 1
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      const invoices = []
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createCard(user)
        const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, plan.id)
        const invoiceid = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, invoices.length ? invoices[0] : null)
        invoices.unshift(invoiceid)
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/customer-invoices?customerid=${user.customer.id}&offset=${offset}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const invoicesNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(invoicesNow[i].id, invoices[offset + i])
      }
    })

    it('should return all records', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      const invoices = []
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createCard(user)
        const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, plan.id)
        const invoiceid = await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, invoices.length ? invoices[0] : null)
        invoices.unshift(invoiceid)
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/customer-invoices?customerid=${user.customer.id}&all=true`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const invoicesNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        assert.strictEqual(invoicesNow[i].id, invoices[i])
      }
    })
  })
})
