const dashboard = require('@userappstore/dashboard')
const stripe = require('stripe')()
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.productid) {
      throw new Error('invalid-productid')
    }
    const product = await global.api.administrator.subscriptions.Product.get(req)
    if (!product) {
      throw new Error('invalid-productid')
    }
  },
  delete: async (req) => {
    try {
      await stripe.products.del(req.query.productid, req.stripeKey)
      await dashboard.RedisList.remove(`${req.appid}:products`, req.query.productid)
      await dashboard.RedisList.remove(`${req.appid}:published:products`, req.query.productid)
      await dashboard.RedisList.remove(`${req.appid}:unpublished:products`, req.query.productid)
      req.success = true
      await stripeCache.delete(req.query.productid)
    } catch (error) {
      throw new Error('unknown-error')
    }
  }
}
