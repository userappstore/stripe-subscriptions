const dashboard = require('@userappstore/dashboard')
const stripe = require('stripe')()
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.couponid) {
      throw new Error('invalid-couponid')
    }
    const coupon = await global.api.administrator.subscriptions.Coupon.get(req)
    if (!coupon) {
      throw new Error('invalid-couponid')
    }
    if (coupon.metadata.published || coupon.metadata.unpublished) {
      throw new Error('invalid-coupon')
    }
  },
  patch: async (req) => {
    const updateInfo = {
      metadata: {
        published: dashboard.Timestamp.now
      }
    }
    try {
      const coupon = await stripe.coupons.update(req.query.couponid, updateInfo, req.stripeKey)
      await stripeCache.update(coupon, req.stripeKey)
      await dashboard.RedisList.add(`${req.appid}:published:coupons`, req.query.couponid)
      req.success = true
      return coupon
    } catch (error) {
      throw new Error('unknown-error')
    }
  }
}
