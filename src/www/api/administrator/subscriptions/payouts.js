const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    req.query = req.query || {}
    let payoutids
    if (req.query.all) {
      payoutids = await dashboard.RedisList.listAll(`${req.appid}:payouts`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      payoutids = await dashboard.RedisList.list(`${req.appid}:payouts`, offset)
    }
    if (!payoutids || !payoutids.length) {
      return null
    }
    const items = []
    for (const payoutid of payoutids) {
      req.query.payoutid = payoutid
      const payout = await global.api.administrator.subscriptions.Payout.get(req)
      items.push(payout)
    }
    return items
  }
}
