const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    req.query = req.query || {}
    let subscriptionids
    if (req.query.all) {
      subscriptionids = await dashboard.RedisList.listAll(`${req.appid}:subscriptions`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      subscriptionids = await dashboard.RedisList.list(`${req.appid}:subscriptions`, offset)
    }
    if (!subscriptionids || !subscriptionids.length) {
      return null
    }
    const items = []
    for (const subscriptionid of subscriptionids) {
      req.query.subscriptionid = subscriptionid
      const subscription = await global.api.administrator.subscriptions.Subscription.get(req)
      items.push(subscription)
    }
    return items
  }
}
