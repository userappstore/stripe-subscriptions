const dashboard = require('@userappstore/dashboard')
const stripe = require('stripe')()

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.customerid) {
      throw new Error('invalid-customerid')
    }
    let cardids
    if (req.query.all) {
      cardids = await dashboard.RedisList.listAll(`${req.appid}:customer:cards:${req.query.customerid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      cardids = await dashboard.RedisList.list(`${req.appid}:customer:cards:${req.query.customerid}`, offset)
    }
    if (!cardids || !cardids.length) {
      return null
    }
    const items = []
    for (const cardid of cardids) {
      const customer = await stripe.customers.retrieveCard(req.query.customerid, cardid, req.stripeKey)
      items.push(customer)
    }
    return items
  }
}
