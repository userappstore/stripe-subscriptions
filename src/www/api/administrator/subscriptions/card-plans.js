const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.cardid) {
      throw new Error('invalid-cardid')
    }
    const exists = await dashboard.RedisList.exists(`${req.appid}:cards`, req.query.cardid)
    if (!exists) {
      throw new Error('invalid-cardid')
    }
    let planids
    if (req.query.all) {
      planids = await dashboard.RedisList.listAll(`${req.appid}:card:plans:${req.query.cardid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      planids = await dashboard.RedisList.list(`${req.appid}:card:plans:${req.query.cardid}`, offset)
    }
    if (!planids || !planids.length) {
      return null
    }
    const items = []
    for (const planid of planids) {
      req.query.planid = planid
      const plan = await global.api.administrator.subscriptions.Plan.get(req)
      items.push(plan)
    }
    return items
  }
}
