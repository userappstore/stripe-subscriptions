/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/administrator/subscriptions/charges', () => {
  describe('Charges#GET', () => {
    it('should limit charges to one page', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        const user = await TestHelper.createUser()
        await TestHelper.createCustomer(user)
        await TestHelper.createCard(user)
        await TestHelper.createSubscription(user, plan.id)
        await TestHelper.waitForNextItem(`subscription:charges:${user.subscription.id}`, null)
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/charges`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const chargesNow = await req.route.api.get(req)
      assert.strictEqual(chargesNow.length, global.pageSize)
    })

    it('should enforce page size', async () => {
      global.pageSize = 3
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
     for (let i = 0, len = global.pageSize + 1; i < len; i++) {
       const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
       const user = await TestHelper.createUser()
       await TestHelper.createCustomer(user)
       await TestHelper.createCard(user)
        await TestHelper.createSubscription(user, plan.id)
        await TestHelper.waitForNextItem(`subscription:charges:${user.subscription.id}`, null)
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/charges`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const chargesNow = await req.route.api.get(req)
      assert.strictEqual(chargesNow.length, global.pageSize)
    })

    it('should enforce specified offset', async () => {
      const offset = 1
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const charges = []
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        const user = await TestHelper.createUser()
        await TestHelper.createCustomer(user)
        await TestHelper.createCard(user)
        await TestHelper.createSubscription(user, plan.id)
        const chargeid = await TestHelper.waitForNextItem(`subscription:charges:${user.subscription.id}`, charges.length ? charges[0] : null)
        charges.unshift(chargeid)
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/charges?offset=${offset}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const chargesNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(chargesNow[i].id, charges[offset + i])
      }
    })

    it('should return all records', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const charges = []
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        const user = await TestHelper.createUser()
        await TestHelper.createCustomer(user)
        await TestHelper.createCard(user)
        await TestHelper.createSubscription(user, plan.id)
        const chargeid = await TestHelper.waitForNextItem(`subscription:charges:${user.subscription.id}`, charges.length ? charges[0] : null)
        charges.unshift(chargeid)
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/charges?all=true`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const chargesNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        assert.strictEqual(chargesNow[i].id, charges[i])
      }
    })
  })
})
