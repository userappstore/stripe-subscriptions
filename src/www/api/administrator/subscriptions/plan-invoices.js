const dashboard = require('@userappstore/dashboard')
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.planid) {
      throw new Error('invalid-planid')
    }
    let invoiceids
    if (req.query.all) {
      invoiceids = await dashboard.RedisList.listAll(`${req.appid}:plan:invoices:${req.query.planid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      invoiceids = await dashboard.RedisList.list(`${req.appid}:plan:invoices:${req.query.planid}`, offset)
    }
    if (!invoiceids || !invoiceids.length) {
      return null
    }
    const items = []
    for (const invoiceid of invoiceids) {
      req.query.invoiceid = invoiceid
      const invoice = await global.api.administrator.subscriptions.Invoice.get(req)
      items.push(invoice)
    }
    return items
  }
}
