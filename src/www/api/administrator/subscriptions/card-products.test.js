/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/administrator/subscriptions/card-products', () => {
  describe('Products#GET', () => {
    it('should limit card\'s products to one page', async () => {
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      let lastid
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const product = await TestHelper.createProduct(administrator, { published: true })
        await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, administrator.plan.id)
        await TestHelper.waitForNextItem(`card:products:${user.card.id}`, lastid)
        lastid = administrator.product.id
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/card-products?cardid=${user.card.id}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const productsNow = await req.route.api.get(req)
      assert.strictEqual(productsNow.length, global.pageSize)
    })

    it('should enforce page size', async () => {
      global.pageSize = 3
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      let lastid
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const product = await TestHelper.createProduct(administrator, { published: true })
        await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, administrator.plan.id)
        await TestHelper.waitForNextItem(`card:products:${user.card.id}`, lastid)
        lastid = administrator.product.id
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/card-products?cardid=${user.card.id}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const productsNow = await req.route.api.get(req)
      assert.strictEqual(productsNow.length, global.pageSize)
    })

    it('should enforce specified offset', async () => {
      const offset = 1
      const administrator = await TestHelper.createAdministrator()
      const products = []
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const product = await TestHelper.createProduct(administrator, { published: true })
        await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, administrator.plan.id)
        await TestHelper.waitForNextItem(`card:products:${user.card.id}`, products.length ? products[0].id : null)
        products.unshift(administrator.product)
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/card-products?cardid=${user.card.id}&offset=${offset}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const productsNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(productsNow[i].id, products[offset + i].id)
      }
    })

    it('should return all records', async () => {
      const administrator = await TestHelper.createAdministrator()
      const products = []
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const product = await TestHelper.createProduct(administrator, { published: true })
        await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, administrator.plan.id)
        await TestHelper.waitForNextItem(`card:products:${user.card.id}`, products.length ? products[0].id : null)
        products.unshift(administrator.product)
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/card-products?cardid=${user.card.id}&all=true`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const productsNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        assert.strictEqual(productsNow[i].id, products[i].id)
      }
    })
  })
})
