const dashboard = require('@userappstore/dashboard')
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.productid) {
      throw new Error('invalid-productid')
    }
    let subscriptionids
    if (req.query.all) {
      subscriptionids = await dashboard.RedisList.listAll(`${req.appid}:product:subscriptions:${req.query.productid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      subscriptionids = await dashboard.RedisList.list(`${req.appid}:product:subscriptions:${req.query.productid}`, offset)
    }
    if (!subscriptionids || !subscriptionids.length) {
      return null
    }
    const items = []
    for (const subscriptionid of subscriptionids) {
      req.query.subscriptionid = subscriptionid
      const subscription = await global.api.administrator.subscriptions.Subscription.get(req)
      items.push(subscription)
    }
    return items
  }
}
