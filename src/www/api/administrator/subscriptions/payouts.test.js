/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe(`/api/administrator/subscriptions/payouts`, () => {
  describe('Payouts#GET', () => {
    it('should return limit payouts to one page', async () => {
      const administrator = await TestHelper.createAdministrator()
      await TestHelper.createPayout()
      const payoutid1 = await TestHelper.waitForNextItem(`payouts`, null)
      const payout2 = await TestHelper.createPayout()
      const payoutid2 = await TestHelper.waitForNextItem(`payouts`, payoutid1)
      const payout3 = await TestHelper.createPayout()
      await TestHelper.waitForNextItem(`payouts`, payoutid2)
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/payouts`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const payouts = await req.route.api.get(req)
      assert.strictEqual(payouts.length, global.pageSize)
      assert.strictEqual(payouts[0].id, payout3.id)
      assert.strictEqual(payouts[1].id, payout2.id)
    })
  })
})
