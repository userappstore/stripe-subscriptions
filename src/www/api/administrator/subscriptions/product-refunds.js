const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.productid) {
      throw new Error('invalid-productid')
    }
    let refundids
    if (req.query.all) {
      refundids = await dashboard.RedisList.listAll(`${req.appid}:product:refunds:${req.query.productid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      refundids = await dashboard.RedisList.list(`${req.appid}:product:refunds:${req.query.productid}`, offset)
    }
    if (!refundids || !refundids.length) {
      return null
    }
    const items = []
    for (const refundid of refundids) {
      req.query.refundid = refundid
      const refund = await global.api.administrator.subscriptions.Refund.get(req)
      items.push(refund)
    }
    return items
  }
}
