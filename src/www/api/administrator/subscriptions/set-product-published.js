const dashboard = require('@userappstore/dashboard')
const stripe = require('stripe')()
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.productid) {
      throw new Error('invalid-productid')
    }
    const product = await global.api.administrator.subscriptions.Product.get(req)
    if (!product) {
      throw new Error('invalid-productid')
    }
    if (product.metadata.published || product.metadata.unpublished) {
      throw new Error('invalid-product')
    }
  },
  patch: async (req) => {
    const updateInfo = {
      metadata: {
        published: dashboard.Timestamp.now
      }
    }
    try {
      const product = await stripe.products.update(req.query.productid, updateInfo, req.stripeKey)
      await stripeCache.update(product, req.stripeKey)
      await dashboard.RedisList.add(`${req.appid}:published:products`, req.query.productid)
      req.success = true
      return product
    } catch (error) {
      throw new Error('unknown-error')
    }
  }
}
