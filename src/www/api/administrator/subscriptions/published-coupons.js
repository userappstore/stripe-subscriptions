const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    req.query = req.query || {}
    let couponids
    if (req.query.all) {
      couponids = await dashboard.RedisList.listAll(`${req.appid}:published:coupons`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      couponids = await dashboard.RedisList.list(`${req.appid}:published:coupons`, offset)
    }
    if (!couponids || !couponids.length) {
      return null
    }
    const items = []
    for (const couponid of couponids) {
      req.query.couponid = couponid
      const item = await global.api.administrator.subscriptions.Coupon.get(req)
      items.push(item)
    }
    return items
  }
}
