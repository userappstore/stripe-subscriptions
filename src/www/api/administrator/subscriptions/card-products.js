const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.cardid) {
      throw new Error('invalid-cardid')
    }
    const exists = await dashboard.RedisList.exists(`${req.appid}:cards`, req.query.cardid)
    if (!exists) {
      throw new Error('invalid-cardid')
    }
    let productids
    if (req.query.all) {
      productids = await dashboard.RedisList.listAll(`${req.appid}:card:products:${req.query.cardid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      productids = await dashboard.RedisList.list(`${req.appid}:card:products:${req.query.cardid}`, offset)
    }
    if (!productids || !productids.length) {
      return null
    }
    const items = []
    for (const productid of productids) {
      req.query.productid = productid
      const product = await global.api.administrator.subscriptions.Product.get(req)
      items.push(product)
    }
    return items
  }
}
