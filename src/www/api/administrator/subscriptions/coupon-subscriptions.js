const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.couponid) {
      throw new Error('invalid-couponid')
    }
    let subscriptionids
    if (req.query.all) {
      subscrpitionids = await dashboard.RedisList.listAll(`${req.appid}:coupon:subscriptions:${req.query.couponid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      subscriptionids = await dashboard.RedisList.list(`${req.appid}:coupon:subscriptions:${req.query.couponid}`, offset)
    }
    if (!subscriptionids || !subscriptionids.length) {
      return null
    }
    const items = []
    for (const subscriptionid of subscriptionids) {
      req.query.subscriptionid = subscriptionid
      const subscription = await global.api.administrator.subscriptions.Subscription.get(req)
      items.push(subscription)
    }
    return items
  }
}
