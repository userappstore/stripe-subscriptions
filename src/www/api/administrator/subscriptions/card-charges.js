const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.cardid) {
      throw new Error('invalid-cardid')
    }
    let chargeids
    if (req.query.all) {
      chargeids = await dashboard.RedisList.listAll(`${req.appid}:card:charges:${req.query.cardid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      chargeids = await dashboard.RedisList.list(`${req.appid}:card:charges:${req.query.cardid}`, offset)
    }
    if (!chargeids || !chargeids.length) {
      return null
    }
    const charges = []
    for (const chargeid of chargeids) {
      req.query.chargeid = chargeid
      const charge = await global.api.administrator.subscriptions.Charge.get(req)
      charges.push(charge)
    }
    return charges
  }
}
