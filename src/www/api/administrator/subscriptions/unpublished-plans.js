const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    req.query = req.query || {}
    let planids
    if (req.query.all) {
      planids = await dashboard.RedisList.listAll(`${req.appid}:unpublished:plans`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      planids = await dashboard.RedisList.list(`${req.appid}:unpublished:plans`, offset)
    }
    if (!planids || !planids.length) {
      return null
    }
    const items = []
    for (const planid of planids) {
      req.query.planid = planid
      const plan = await global.api.administrator.subscriptions.Plan.get(req)
      items.push(plan)
    }
    return items
  }
}
