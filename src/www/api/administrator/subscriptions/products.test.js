/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/administrator/subscriptions/products', () => {
  describe('Plans#GET', () => {
    it('should limit products to one page', async () => {
      const administrator = await TestHelper.createAdministrator()
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createProduct(administrator, { published: true })
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/products`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const productsNow = await req.route.api.get(req)
      assert.strictEqual(productsNow.length, global.pageSize)
    })

    it('should enforce page size', async () => {
      global.pageSize = 3
      const administrator = await TestHelper.createAdministrator()
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createProduct(administrator, { published: true, unpublished: true })
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/products`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const productsNow = await req.route.api.get(req)
      assert.strictEqual(productsNow.length, global.pageSize)
    })

    it('should enforce specified offset', async () => {
      const offset = 1
      const administrator = await TestHelper.createAdministrator()
      const products = []
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const product = await TestHelper.createProduct(administrator, { })
        products.unshift(product)
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/products?offset=${offset}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const productsNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(productsNow[i].id, products[offset + i].id)
      }
    })

    it('should return all records', async () => {
      const administrator = await TestHelper.createAdministrator()
      const products = []
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const product = await TestHelper.createProduct(administrator, { published: true })
        products.unshift(product)
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/products?all=true`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const productsNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        assert.strictEqual(productsNow[i].id, products[i].id)
      }
    })
  })
})
