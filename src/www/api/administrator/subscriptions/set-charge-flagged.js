const stripe = require('stripe')()
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.chargeid) {
      throw new Error('invalid-chargeid')
    }
    const charge= await global.api.administrator.subscriptions.Charge.get(req)
    if (!charge) {
      throw new Error('invalid-chargeid')
    }
    if (!charge.refunded || charge.fraud_details.user_report) {
      throw new Error('invalid-charge')
    }
  },
  patch: async (req) => {
    const chargeInfo = {
      fraud_details: {
        user_report: 'fraudulent'
      }
    }
    try {
      const charge = await stripe.charges.update(req.query.chargeid, chargeInfo, req.stripeKey)
      await stripeCache.update(charge, req.stripeKey)
      req.success = true
      return charge
    } catch (error) {
      if (error.message.indexOf('is greater than')) {
        throw new Error('invalid-amount')
      }
      throw new Error('unknown-error')
    }
  }
}
