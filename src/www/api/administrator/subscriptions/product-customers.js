const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.productid) {
      throw new Error('invalid-productid')
    }
    let customerids
    if (req.query.all) {
      customerids = await dashboard.RedisList.listAll(`${req.appid}:product:customers:${req.query.productid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      customerids = await dashboard.RedisList.list(`${req.appid}:product:customers:${req.query.productid}`, offset)
    }
    if (!customerids || !customerids.length) {
      return null
    }
    const items = []
    for (const customerid of customerids) {
      req.query.customerid = customerid
      const item = await global.api.administrator.subscriptions.Customer.get(req)
      items.push(item)
    }
    return items
  }
}
