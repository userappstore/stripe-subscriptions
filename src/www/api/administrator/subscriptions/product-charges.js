const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.productid) {
      throw new Error('invalid-productid')
    }
    let chargeids
    if (req.query.all) {
      chargeids = await dashboard.RedisList.listAll(`${req.appid}:product:charges:${req.query.productid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      chargeids = await dashboard.RedisList.list(`${req.appid}:product:charges:${req.query.productid}`, offset)
    }
    if (!chargeids || !chargeids.length) {
      return null
    }
    const items = []
    for (const chargeid of chargeids) {
      req.query.chargeid = chargeid
      const charge = await global.api.administrator.subscriptions.Charge.get(req)
      items.push(charge)
    }
    return items
  }
}
