const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.planid) {
      throw new Error('invalid-planid')
    }
    let chargeids
    if (req.query.all) {
      chargeids = await dashboard.RedisList.listAll(`${req.appid}:plan:charges:${req.query.planid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      chargeids = await dashboard.RedisList.list(`${req.appid}:plan:charges:${req.query.planid}`, offset)
    }
    if (!chargeids || !chargeids.length) {
      return null
    }
    const items = []
    for (const chargeid of chargeids) {
      req.query.chargeid = chargeid
      const charge = await global.api.administrator.subscriptions.Charge.get(req)
      items.push(charge)
    }
    return items
  }
}
