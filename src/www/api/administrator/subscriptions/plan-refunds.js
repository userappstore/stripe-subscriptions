const dashboard = require('@userappstore/dashboard')
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.planid) {
      throw new Error('invalid-planid')
    }
    let refundids
    if (req.query.all) {
      refundids = await dashboard.RedisList.listAll(`${req.appid}:plan:refunds:${req.query.planid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      refundids = await dashboard.RedisList.list(`${req.appid}:plan:refunds:${req.query.planid}`, offset)
    }
    if (!refundids || !refundids.length) {
      return null
    }
    const items = []
    for (const refundid of refundids) {
      req.query.refundid = refundid
      const refund = await global.api.administrator.subscriptions.Refund.get(req)
      items.push(refund)
    }
    return items
  }
}
