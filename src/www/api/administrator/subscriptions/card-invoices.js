const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.cardid) {
      throw new Error('invalid-cardid')
    }
    const exists = await dashboard.RedisList.exists(`${req.appid}:cards`, req.query.cardid)
    if (!exists) {
      throw new Error('invalid-cardid')
    }
    let invoiceids
    if (req.query.all) {
      invoiceids = await dashboard.RedisList.listAll(`${req.appid}:card:invoices:${req.query.cardid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      invoiceids = await dashboard.RedisList.list(`${req.appid}:card:invoices:${req.query.cardid}`, offset)
    }
    if (!invoiceids || !invoiceids.length) {
      return null
    }
    const items = []
    for (const invoiceid of invoiceids) {
      req.query.invoiceid = invoiceid
      const invoice = await global.api.administrator.subscriptions.Invoice.get(req)
      items.push(invoice)
    }
    return items
  }
}
