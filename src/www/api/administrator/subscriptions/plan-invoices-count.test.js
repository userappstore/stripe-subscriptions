/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/administrator/subscriptions/plan-invoices-count', async () => {
  describe('PlanInvoicesCount#GET', () => {
    it('should count all invoices on plan', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      await TestHelper.createSubscription(user, administrator.plan.id)
      await TestHelper.waitForNextItem(`subscription:invoices:${user.subscription.id}`, null)
      const user2 = await TestHelper.createUser()
      await TestHelper.createCustomer(user2)
      await TestHelper.createCard(user2)
      await TestHelper.createSubscription(user2, administrator.plan.id)
      await TestHelper.waitForNextItem(`subscription:invoices:${user2.subscription.id}`, null)
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/plan-invoices-count?planid=${administrator.plan.id}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const result = await req.route.api.get(req)
      assert.strictEqual(result, 2)
    })
  })
})
