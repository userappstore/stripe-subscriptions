const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.couponid) {
      throw new Error('invalid-couponid')
    }
    let customerids
    if (req.query.all) {
      customerids = await dashboard.RedisList.listAll(`${req.appid}:coupon:customers:${req.query.couponid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      customerids = await dashboard.RedisList.list(`${req.appid}:coupon:customers:${req.query.couponid}`, offset)
    }
    if (!customerids || !customerids.length) {
      return null
    }
    const items = []
    for (const customerid of customerids) {
      req.query.customerid = customerid
      const item = await global.api.administrator.subscriptions.Customer.get(req)
      items.push(item)
    }
    return items
  }
}
