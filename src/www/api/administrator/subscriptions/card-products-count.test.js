/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/administrator/subscriptions/card-products-count', async () => {
  describe('CardProductsCount#GET', () => {
    it('should count all products on card', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product1 = await TestHelper.createProduct(administrator, { published: true })
      const plan1 = await TestHelper.createPlan(administrator, { productid: product1.id, published: true, trial_period_days: 0, amount: 1000 })
      const product2 = await TestHelper.createProduct(administrator, { published: true })
      const plan2 = await TestHelper.createPlan(administrator, { productid: product2.id, published: true, trial_period_days: 0, amount: 1000 })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      await TestHelper.createSubscription(user, plan1.id)
      const invoiceid1 = await TestHelper.waitForNextItem(`card:invoices:${user.card.id}`, null)
      await TestHelper.createSubscription(user, plan2.id)
      await TestHelper.waitForNextItem(`card:invoices:${user.card.id}`, invoiceid1)
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/card-products-count?cardid=${user.card.id}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const result = await req.route.api.get(req)
      assert.strictEqual(result, 2)
    })
  })
})
