const dashboard = require('@userappstore/dashboard')
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  get: async (req) => {
    req.query = req.query || {}
    let customerids
    if (req.query.all) {
      customerids = await dashboard.RedisList.listAll(`${req.appid}:customers`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      customerids = await dashboard.RedisList.list(`${req.appid}:customers`, offset)
    }
    if (!customerids || !customerids.length) {
      return null
    }
    const items = []
    for (const customerid of customerids) {
      req.query.customerid = customerid
      const item = await global.api.administrator.subscriptions.Customer.get(req)
      items.push(item)
    }
    return items
  }
}
