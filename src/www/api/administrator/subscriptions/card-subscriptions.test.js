/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/administrator/subscriptions/card-subscriptions', () => {
  describe('CardSubscriptions#GET', () => {
    it('should limit subscriptions on card to one page', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      let lastid
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, plan.id)
        lastid = await TestHelper.waitForNextItem(`account:subscriptions:${user.account.accountid}`, lastid)
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/card-subscriptions?cardid=${user.card.id}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const subscriptions = await req.route.api.get(req)
      assert.strictEqual(subscriptions.length, global.pageSize)
    })

    it('should enforce page size', async () => {
      global.pageSize = 3
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      let lastid
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, plan.id)
        lastid = await TestHelper.waitForNextItem(`account:subscriptions:${user.account.accountid}`, lastid)
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/card-subscriptions?cardid=${user.card.id}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const subscriptions = await req.route.api.get(req)
      assert.strictEqual(subscriptions.length, global.pageSize)
    })

    it('should enforce specified offset', async () => {
      const offset = 1
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      const subscriptions = []
      for (let i = 0, len = offset + global.pageSize + 1; i < len; i++) {
        const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, plan.id)
        await TestHelper.waitForNextItem(`account:subscriptions:${user.account.accountid}`, subscriptions.length ? subscriptions[0].id : null)
        subscriptions.unshift(user.subscription)
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/card-subscriptions?cardid=${user.card.id}&offset=${offset}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const subscriptionsNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(subscriptionsNow[i].id, subscriptions[offset + i].id)
      }
    })

    it('should return all records', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      const subscriptions = []
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const plan = await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, plan.id)
        await TestHelper.waitForNextItem(`account:subscriptions:${user.account.accountid}`, subscriptions.length ? subscriptions[0].id : null)
        subscriptions.unshift(user.subscription)
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/card-subscriptions?cardid=${user.card.id}&all=true`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const subscriptionsNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        assert.strictEqual(subscriptionsNow[i].id, subscriptions[i].id)
      }
    })
  })
})
