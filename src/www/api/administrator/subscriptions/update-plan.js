const stripe = require('stripe')()
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.planid) {
      throw new Error('invalid-planid')
    }
    if (!req.body || !req.body.productid) {
      throw new Error('invalid-productid')
    }
    const plan = await global.api.administrator.subscriptions.Plan.get(req)
    if (!plan) {
      throw new Error('invalid-planid')
    }
    if (plan.metadata.unpublished) {
      throw new Error('invalid-plan')
    }
    req.query.productid = req.body.productid
    const product = await global.api.administrator.subscriptions.Product.get(req)
    if (!product) {
      throw new Error('invalid-productid')
    }
    if (!product.metadata.published || product.metadata.unpublished) {
      throw new Error('invalid-product')
    }
    if (req.body.trial_period_days) {
      try {
        const trialPeriodDays = parseInt(req.body.trial_period_days, 10)
        if (trialPeriodDays !== req.body.trial_period_days.toString()) {
          throw new Error('invalid-trial_period_days')
        }
      } catch (s) {
        throw new Error('invalid-trial_period_days')
      }
    }
  },
  patch: async (req) => {
    const updateInfo = {}
    if (req.body.productid) {
      updateInfo.product = req.body.productid
    }
    if (req.body.trial_period_days) {
      updateInfo.trial_period_days = req.body.trial_period_days
    }
    try {
      const plan = await stripe.plans.update(req.query.planid, updateInfo, req.stripeKey)
      await stripeCache.update(plan, req.stripeKey)
      req.success = true
      return plan
    } catch (error) {
      throw new Error('unknown-error')
    }
  }
}
