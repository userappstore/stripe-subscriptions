
const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.planid) {
      throw new Error('invalid-planid')
    }
    let subscriptionids
    if (req.query.all) {
      subscriptionids = await dashboard.RedisList.listAll(`${req.appid}:plan:subscriptions:${req.query.planid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      subscriptionids = await dashboard.RedisList.list(`${req.appid}:plan:subscriptions:${req.query.planid}`, offset)
    }
    if (!subscriptionids || !subscriptionids.length) {
      return null
    }
    const items = []
    for (const subscriptionid of subscriptionids) {
      req.query.subscriptionid = subscriptionid
      const item = await global.api.administrator.subscriptions.Subscription.get(req)
      items.push(item)
    }
    return items
  }
}
