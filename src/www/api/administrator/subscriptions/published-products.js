const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    req.query = req.query || {}
    let productids
    if (req.query.all) {
      productids = await dashboard.RedisList.listAll(`${req.appid}:published:products`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      productids = await dashboard.RedisList.list(`${req.appid}:published:products`, offset)
    }
    if (!productids || !productids.length) {
      return null
    }
    const items = []
    for (const productid of productids) {
      req.query.productid = productid
      const product = await global.api.administrator.subscriptions.Product.get(req)
      items.push(product)
    }
    return items
  }
}
