/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/administrator/subscriptions/card-plans', () => {
  describe('Plans#GET', () => {
    it('should limit card\'s plans to one page', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      let lastid
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, administrator.plan.id)
        await TestHelper.waitForNextItem(`card:plans:${user.card.id}`, lastid)
        lastid = administrator.plan.id
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/card-plans?cardid=${user.card.id}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const plansNow = await req.route.api.get(req)
      assert.strictEqual(plansNow.length, global.pageSize)
    })

    it('should enforce page size', async () => {
      global.pageSize = 3
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      let lastid
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, administrator.plan.id)
        await TestHelper.waitForNextItem(`card:plans:${user.card.id}`, lastid)
        lastid = administrator.plan.id
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/card-plans?cardid=${user.card.id}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const plansNow = await req.route.api.get(req)
      assert.strictEqual(plansNow.length, global.pageSize)
    })

    it('should enforce specified offset', async () => {
      const offset = 1
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const plans = []
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, administrator.plan.id)
        await TestHelper.waitForNextItem(`card:plans:${user.card.id}`, plans.length ? plans[0].id : null)
        plans.unshift(administrator.plan)
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/card-plans?cardid=${user.card.id}&offset=${offset}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const plansNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(plansNow[i].id, plans[offset + i].id)
      }
    })

    it('should return all records', async () => {
      const administrator = await TestHelper.createAdministrator()
      const product = await TestHelper.createProduct(administrator, { published: true })
      const plans = []
      const user = await TestHelper.createUser()
      await TestHelper.createCustomer(user)
      await TestHelper.createCard(user)
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createPlan(administrator, { productid: product.id, published: true, trial_period_days: 0, amount: 1000 })
        await TestHelper.createSubscription(user, administrator.plan.id)
        await TestHelper.waitForNextItem(`card:plans:${user.card.id}`, plans.length ? plans[0].id : null)
        plans.unshift(administrator.plan)
      }
      const req = TestHelper.createRequest(`/api/administrator/subscriptions/card-plans?cardid=${user.card.id}&all=true`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const plansNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        assert.strictEqual(plansNow[i].id, plans[i].id)
      }
    })
  })
})
