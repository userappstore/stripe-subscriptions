const stripe = require('stripe')()
const stripeCache = require('../../../../stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.customerid) {
      throw new Error('invalid-customerid')
    }
    const customer = await global.api.administrator.subscriptions.Customer.get(req)
    if (!customer) {
      throw new Error('invalid-customerid')
    }
    if (!customer.discount) {
      throw new Error('invalid-customer')
    }
  },
  delete: async (req) => {
    const updateInfo = {
      coupon: null
    }
    try {
      const customer = await stripe.customers.update(req.query.customerid, updateInfo, req.stripeKey)
      await stripeCache.update(customer, req.stripeKey)
      req.success = true
      return customer
    } catch (error) {
      throw new Error('unknown-error')
    }
  }
}
