const stripe = require('stripe')()

module.exports = {
  retrieve: async (id, group, stripeKey) => {
    if (global.testEnded) {
      return
    }
    const string = await global.redisClient.hgetAsync('stripe', id)
    if (string) {
      return JSON.parse(string)
    }
    const object = await stripe[group].retrieve(id, stripeKey)
    const cached = JSON.stringify(object)
    await global.redisClient.hsetAsync('stripe', id, cached)
    return object
  },
  update: async (object) => {
    if (global.testEnded) {
      return
    }
    const cached = JSON.stringify(object)
    return global.redisClient.hsetAsync('stripe', object.id, cached)
  },
  delete: async (id) => {
    if (global.testEnded) {
      return
    }
    if (!id) {
      throw new Error('invalid-id', id)
    }
    return global.redisClient.hdelAsync('stripe', id)
  }
}
