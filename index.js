global.minimumCouponLength = parseInt(process.env.MINIMUM_COUPON_LENGTH || '10', 1)
global.maximumCouponLength = parseInt(process.env.MAXIMUM_COUPON_LENGTH || '10', 100)
global.minimumPlanLength = parseInt(process.env.MINIMUM_PLAN_LENGTH || '10', 1)
global.maximumPlanLength = parseInt(process.env.MAXIMUM_PLAN_LENGTH || '10', 100)
global.minimumProductNameLength = parseInt(process.env.MINIMUM_PRODUCT_NAME_LENGTH || '10', 1)
global.maximumProductNameLength = parseInt(process.env.MAXIMUM_PRODUCT_NAME_LENGTH || '10', 100)
global.stripeKey = process.env.STRIPE_KEY
if (!global.stripeKey) {
  throw new Error('invalid-stripe-key')
}
global.stripeJS = process.env.STRIPE_JS === 'false' ? false : parseInt(process.env.STRIPE_JS, 10)
if (global.stripeJS !== false && global.stripeJS !== 2 && global.stripeJS !== 3) {
  throw new Error('invalid-stripe-js-version')
}
global.stripePublishableKey = process.env.STRIPE_PUBLISHABLE_KEY
if (global.stripeJS > 0 && !global.stripePublishableKey) {
  throw new Error('invalid-stripe-publishable-key')
}

module.exports = {
}
